import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ElementoComponent } from 'app/entities/elemento/elemento.component';
import { ElementoService } from 'app/entities/elemento/elemento.service';
import { Elemento } from 'app/shared/model/elemento.model';

describe('Component Tests', () => {
  describe('Elemento Management Component', () => {
    let comp: ElementoComponent;
    let fixture: ComponentFixture<ElementoComponent>;
    let service: ElementoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ElementoComponent],
      })
        .overrideTemplate(ElementoComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ElementoComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ElementoService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Elemento(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.elementos && comp.elementos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
