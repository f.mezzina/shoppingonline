import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ElementoUpdateComponent } from 'app/entities/elemento/elemento-update.component';
import { ElementoService } from 'app/entities/elemento/elemento.service';
import { Elemento } from 'app/shared/model/elemento.model';

describe('Component Tests', () => {
  describe('Elemento Management Update Component', () => {
    let comp: ElementoUpdateComponent;
    let fixture: ComponentFixture<ElementoUpdateComponent>;
    let service: ElementoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ElementoUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ElementoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ElementoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ElementoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Elemento(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Elemento();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
