import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ElementoDetailComponent } from 'app/entities/elemento/elemento-detail.component';
import { Elemento } from 'app/shared/model/elemento.model';

describe('Component Tests', () => {
  describe('Elemento Management Detail Component', () => {
    let comp: ElementoDetailComponent;
    let fixture: ComponentFixture<ElementoDetailComponent>;
    const route = ({ data: of({ elemento: new Elemento(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ElementoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ElementoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ElementoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load elemento on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.elemento).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
