import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ConfigurazioneDetailComponent } from 'app/entities/configurazione/configurazione-detail.component';
import { Configurazione } from 'app/shared/model/configurazione.model';

describe('Component Tests', () => {
  describe('Configurazione Management Detail Component', () => {
    let comp: ConfigurazioneDetailComponent;
    let fixture: ComponentFixture<ConfigurazioneDetailComponent>;
    const route = ({ data: of({ configurazione: new Configurazione(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ConfigurazioneDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ConfigurazioneDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ConfigurazioneDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load configurazione on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.configurazione).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
