import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ConfigurazioneUpdateComponent } from 'app/entities/configurazione/configurazione-update.component';
import { ConfigurazioneService } from 'app/entities/configurazione/configurazione.service';
import { Configurazione } from 'app/shared/model/configurazione.model';

describe('Component Tests', () => {
  describe('Configurazione Management Update Component', () => {
    let comp: ConfigurazioneUpdateComponent;
    let fixture: ComponentFixture<ConfigurazioneUpdateComponent>;
    let service: ConfigurazioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ConfigurazioneUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ConfigurazioneUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ConfigurazioneUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ConfigurazioneService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Configurazione(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Configurazione();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
