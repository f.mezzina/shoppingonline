import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ConfigurazioneComponent } from 'app/entities/configurazione/configurazione.component';
import { ConfigurazioneService } from 'app/entities/configurazione/configurazione.service';
import { Configurazione } from 'app/shared/model/configurazione.model';

describe('Component Tests', () => {
  describe('Configurazione Management Component', () => {
    let comp: ConfigurazioneComponent;
    let fixture: ComponentFixture<ConfigurazioneComponent>;
    let service: ConfigurazioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ConfigurazioneComponent],
      })
        .overrideTemplate(ConfigurazioneComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ConfigurazioneComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ConfigurazioneService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Configurazione(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.configuraziones && comp.configuraziones[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
