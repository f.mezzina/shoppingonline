import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ComputerDetailComponent } from 'app/entities/computer/computer-detail.component';
import { Computer } from 'app/shared/model/computer.model';

describe('Component Tests', () => {
  describe('Computer Management Detail Component', () => {
    let comp: ComputerDetailComponent;
    let fixture: ComponentFixture<ComputerDetailComponent>;
    const route = ({ data: of({ computer: new Computer(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ComputerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ComputerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ComputerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load computer on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.computer).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
