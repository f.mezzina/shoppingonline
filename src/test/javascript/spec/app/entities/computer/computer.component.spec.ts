import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ComputerComponent } from 'app/entities/computer/computer.component';
import { ComputerService } from 'app/entities/computer/computer.service';
import { Computer } from 'app/shared/model/computer.model';

describe('Component Tests', () => {
  describe('Computer Management Component', () => {
    let comp: ComputerComponent;
    let fixture: ComponentFixture<ComputerComponent>;
    let service: ComputerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ComputerComponent],
      })
        .overrideTemplate(ComputerComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ComputerComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ComputerService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Computer(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.computers && comp.computers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
