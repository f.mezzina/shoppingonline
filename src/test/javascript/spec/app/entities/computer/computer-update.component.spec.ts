import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ComputerUpdateComponent } from 'app/entities/computer/computer-update.component';
import { ComputerService } from 'app/entities/computer/computer.service';
import { Computer } from 'app/shared/model/computer.model';

describe('Component Tests', () => {
  describe('Computer Management Update Component', () => {
    let comp: ComputerUpdateComponent;
    let fixture: ComponentFixture<ComputerUpdateComponent>;
    let service: ComputerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ComputerUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ComputerUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ComputerUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ComputerService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Computer(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Computer();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
