import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { SpedizioneDetailComponent } from 'app/entities/spedizione/spedizione-detail.component';
import { Spedizione } from 'app/shared/model/spedizione.model';

describe('Component Tests', () => {
  describe('Spedizione Management Detail Component', () => {
    let comp: SpedizioneDetailComponent;
    let fixture: ComponentFixture<SpedizioneDetailComponent>;
    const route = ({ data: of({ spedizione: new Spedizione(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [SpedizioneDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(SpedizioneDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpedizioneDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load spedizione on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.spedizione).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
