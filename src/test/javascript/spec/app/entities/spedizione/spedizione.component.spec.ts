import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { SpedizioneComponent } from 'app/entities/spedizione/spedizione.component';
import { SpedizioneService } from 'app/entities/spedizione/spedizione.service';
import { Spedizione } from 'app/shared/model/spedizione.model';

describe('Component Tests', () => {
  describe('Spedizione Management Component', () => {
    let comp: SpedizioneComponent;
    let fixture: ComponentFixture<SpedizioneComponent>;
    let service: SpedizioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [SpedizioneComponent],
      })
        .overrideTemplate(SpedizioneComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpedizioneComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpedizioneService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Spedizione(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.spediziones && comp.spediziones[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
