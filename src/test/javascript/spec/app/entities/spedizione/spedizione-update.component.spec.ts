import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { SpedizioneUpdateComponent } from 'app/entities/spedizione/spedizione-update.component';
import { SpedizioneService } from 'app/entities/spedizione/spedizione.service';
import { Spedizione } from 'app/shared/model/spedizione.model';

describe('Component Tests', () => {
  describe('Spedizione Management Update Component', () => {
    let comp: SpedizioneUpdateComponent;
    let fixture: ComponentFixture<SpedizioneUpdateComponent>;
    let service: SpedizioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [SpedizioneUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(SpedizioneUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpedizioneUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpedizioneService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Spedizione(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Spedizione();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
