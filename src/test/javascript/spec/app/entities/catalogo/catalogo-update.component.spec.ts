import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { CatalogoUpdateComponent } from 'app/entities/catalogo/catalogo-update.component';
import { CatalogoService } from 'app/entities/catalogo/catalogo.service';
import { Catalogo } from 'app/shared/model/catalogo.model';

describe('Component Tests', () => {
  describe('Catalogo Management Update Component', () => {
    let comp: CatalogoUpdateComponent;
    let fixture: ComponentFixture<CatalogoUpdateComponent>;
    let service: CatalogoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [CatalogoUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CatalogoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CatalogoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CatalogoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Catalogo(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Catalogo();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
