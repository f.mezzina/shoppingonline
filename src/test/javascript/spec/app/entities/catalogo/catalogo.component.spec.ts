import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { CatalogoComponent } from 'app/entities/catalogo/catalogo.component';
import { CatalogoService } from 'app/entities/catalogo/catalogo.service';
import { Catalogo } from 'app/shared/model/catalogo.model';

describe('Component Tests', () => {
  describe('Catalogo Management Component', () => {
    let comp: CatalogoComponent;
    let fixture: ComponentFixture<CatalogoComponent>;
    let service: CatalogoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [CatalogoComponent],
      })
        .overrideTemplate(CatalogoComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CatalogoComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CatalogoService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Catalogo(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.catalogos && comp.catalogos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
