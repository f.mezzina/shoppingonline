import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { CatalogoDetailComponent } from 'app/entities/catalogo/catalogo-detail.component';
import { Catalogo } from 'app/shared/model/catalogo.model';

describe('Component Tests', () => {
  describe('Catalogo Management Detail Component', () => {
    let comp: CatalogoDetailComponent;
    let fixture: ComponentFixture<CatalogoDetailComponent>;
    const route = ({ data: of({ catalogo: new Catalogo(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [CatalogoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CatalogoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CatalogoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load catalogo on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.catalogo).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
