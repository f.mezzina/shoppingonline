import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { FatturazioneUpdateComponent } from 'app/entities/fatturazione/fatturazione-update.component';
import { FatturazioneService } from 'app/entities/fatturazione/fatturazione.service';
import { Fatturazione } from 'app/shared/model/fatturazione.model';

describe('Component Tests', () => {
  describe('Fatturazione Management Update Component', () => {
    let comp: FatturazioneUpdateComponent;
    let fixture: ComponentFixture<FatturazioneUpdateComponent>;
    let service: FatturazioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [FatturazioneUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FatturazioneUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FatturazioneUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FatturazioneService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Fatturazione(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Fatturazione();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
