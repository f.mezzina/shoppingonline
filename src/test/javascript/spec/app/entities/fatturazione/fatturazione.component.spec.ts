import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { FatturazioneComponent } from 'app/entities/fatturazione/fatturazione.component';
import { FatturazioneService } from 'app/entities/fatturazione/fatturazione.service';
import { Fatturazione } from 'app/shared/model/fatturazione.model';

describe('Component Tests', () => {
  describe('Fatturazione Management Component', () => {
    let comp: FatturazioneComponent;
    let fixture: ComponentFixture<FatturazioneComponent>;
    let service: FatturazioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [FatturazioneComponent],
      })
        .overrideTemplate(FatturazioneComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FatturazioneComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FatturazioneService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Fatturazione(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fatturaziones && comp.fatturaziones[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
