import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { FatturazioneDetailComponent } from 'app/entities/fatturazione/fatturazione-detail.component';
import { Fatturazione } from 'app/shared/model/fatturazione.model';

describe('Component Tests', () => {
  describe('Fatturazione Management Detail Component', () => {
    let comp: FatturazioneDetailComponent;
    let fixture: ComponentFixture<FatturazioneDetailComponent>;
    const route = ({ data: of({ fatturazione: new Fatturazione(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [FatturazioneDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FatturazioneDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FatturazioneDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fatturazione on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fatturazione).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
