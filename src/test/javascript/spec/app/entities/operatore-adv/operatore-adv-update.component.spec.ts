import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { OperatoreAdvUpdateComponent } from 'app/entities/operatore-adv/operatore-adv-update.component';
import { OperatoreAdvService } from 'app/entities/operatore-adv/operatore-adv.service';
import { OperatoreAdv } from 'app/shared/model/operatore-adv.model';

describe('Component Tests', () => {
  describe('OperatoreAdv Management Update Component', () => {
    let comp: OperatoreAdvUpdateComponent;
    let fixture: ComponentFixture<OperatoreAdvUpdateComponent>;
    let service: OperatoreAdvService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [OperatoreAdvUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(OperatoreAdvUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OperatoreAdvUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OperatoreAdvService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OperatoreAdv(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OperatoreAdv();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
