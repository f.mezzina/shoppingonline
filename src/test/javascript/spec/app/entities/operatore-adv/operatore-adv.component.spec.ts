import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { OperatoreAdvComponent } from 'app/entities/operatore-adv/operatore-adv.component';
import { OperatoreAdvService } from 'app/entities/operatore-adv/operatore-adv.service';
import { OperatoreAdv } from 'app/shared/model/operatore-adv.model';

describe('Component Tests', () => {
  describe('OperatoreAdv Management Component', () => {
    let comp: OperatoreAdvComponent;
    let fixture: ComponentFixture<OperatoreAdvComponent>;
    let service: OperatoreAdvService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [OperatoreAdvComponent],
      })
        .overrideTemplate(OperatoreAdvComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OperatoreAdvComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OperatoreAdvService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new OperatoreAdv(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.operatoreAdvs && comp.operatoreAdvs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
