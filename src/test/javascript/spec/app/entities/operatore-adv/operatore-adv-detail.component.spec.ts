import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { OperatoreAdvDetailComponent } from 'app/entities/operatore-adv/operatore-adv-detail.component';
import { OperatoreAdv } from 'app/shared/model/operatore-adv.model';

describe('Component Tests', () => {
  describe('OperatoreAdv Management Detail Component', () => {
    let comp: OperatoreAdvDetailComponent;
    let fixture: ComponentFixture<OperatoreAdvDetailComponent>;
    const route = ({ data: of({ operatoreAdv: new OperatoreAdv(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [OperatoreAdvDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OperatoreAdvDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OperatoreAdvDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load operatoreAdv on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.operatoreAdv).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
