import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { FatturaComponent } from 'app/entities/fattura/fattura.component';
import { FatturaService } from 'app/entities/fattura/fattura.service';
import { Fattura } from 'app/shared/model/fattura.model';

describe('Component Tests', () => {
  describe('Fattura Management Component', () => {
    let comp: FatturaComponent;
    let fixture: ComponentFixture<FatturaComponent>;
    let service: FatturaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [FatturaComponent],
      })
        .overrideTemplate(FatturaComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FatturaComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FatturaService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Fattura(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fatturas && comp.fatturas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
