import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { FatturaDetailComponent } from 'app/entities/fattura/fattura-detail.component';
import { Fattura } from 'app/shared/model/fattura.model';

describe('Component Tests', () => {
  describe('Fattura Management Detail Component', () => {
    let comp: FatturaDetailComponent;
    let fixture: ComponentFixture<FatturaDetailComponent>;
    const route = ({ data: of({ fattura: new Fattura(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [FatturaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FatturaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FatturaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fattura on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fattura).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
