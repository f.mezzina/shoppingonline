import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { FatturaUpdateComponent } from 'app/entities/fattura/fattura-update.component';
import { FatturaService } from 'app/entities/fattura/fattura.service';
import { Fattura } from 'app/shared/model/fattura.model';

describe('Component Tests', () => {
  describe('Fattura Management Update Component', () => {
    let comp: FatturaUpdateComponent;
    let fixture: ComponentFixture<FatturaUpdateComponent>;
    let service: FatturaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [FatturaUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FatturaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FatturaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FatturaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Fattura(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Fattura();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
