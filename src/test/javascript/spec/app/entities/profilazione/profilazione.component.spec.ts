import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ProfilazioneComponent } from 'app/entities/profilazione/profilazione.component';
import { ProfilazioneService } from 'app/entities/profilazione/profilazione.service';
import { Profilazione } from 'app/shared/model/profilazione.model';

describe('Component Tests', () => {
  describe('Profilazione Management Component', () => {
    let comp: ProfilazioneComponent;
    let fixture: ComponentFixture<ProfilazioneComponent>;
    let service: ProfilazioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ProfilazioneComponent],
      })
        .overrideTemplate(ProfilazioneComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProfilazioneComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProfilazioneService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Profilazione(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.profilaziones && comp.profilaziones[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
