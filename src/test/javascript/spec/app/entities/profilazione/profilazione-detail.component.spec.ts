import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ProfilazioneDetailComponent } from 'app/entities/profilazione/profilazione-detail.component';
import { Profilazione } from 'app/shared/model/profilazione.model';

describe('Component Tests', () => {
  describe('Profilazione Management Detail Component', () => {
    let comp: ProfilazioneDetailComponent;
    let fixture: ComponentFixture<ProfilazioneDetailComponent>;
    const route = ({ data: of({ profilazione: new Profilazione(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ProfilazioneDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProfilazioneDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProfilazioneDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load profilazione on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.profilazione).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
