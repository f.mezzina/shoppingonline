import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { ProfilazioneUpdateComponent } from 'app/entities/profilazione/profilazione-update.component';
import { ProfilazioneService } from 'app/entities/profilazione/profilazione.service';
import { Profilazione } from 'app/shared/model/profilazione.model';

describe('Component Tests', () => {
  describe('Profilazione Management Update Component', () => {
    let comp: ProfilazioneUpdateComponent;
    let fixture: ComponentFixture<ProfilazioneUpdateComponent>;
    let service: ProfilazioneService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [ProfilazioneUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProfilazioneUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProfilazioneUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProfilazioneService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Profilazione(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Profilazione();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
