import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { CommessaUpdateComponent } from 'app/entities/commessa/commessa-update.component';
import { CommessaService } from 'app/entities/commessa/commessa.service';
import { Commessa } from 'app/shared/model/commessa.model';

describe('Component Tests', () => {
  describe('Commessa Management Update Component', () => {
    let comp: CommessaUpdateComponent;
    let fixture: ComponentFixture<CommessaUpdateComponent>;
    let service: CommessaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [CommessaUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CommessaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CommessaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommessaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Commessa(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Commessa();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
