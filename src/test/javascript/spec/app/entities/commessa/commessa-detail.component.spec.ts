import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ShoppingonlineTestModule } from '../../../test.module';
import { CommessaDetailComponent } from 'app/entities/commessa/commessa-detail.component';
import { Commessa } from 'app/shared/model/commessa.model';

describe('Component Tests', () => {
  describe('Commessa Management Detail Component', () => {
    let comp: CommessaDetailComponent;
    let fixture: ComponentFixture<CommessaDetailComponent>;
    const route = ({ data: of({ commessa: new Commessa(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [CommessaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CommessaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CommessaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load commessa on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.commessa).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
