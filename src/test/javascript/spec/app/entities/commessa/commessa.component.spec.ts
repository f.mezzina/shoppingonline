import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ShoppingonlineTestModule } from '../../../test.module';
import { CommessaComponent } from 'app/entities/commessa/commessa.component';
import { CommessaService } from 'app/entities/commessa/commessa.service';
import { Commessa } from 'app/shared/model/commessa.model';

describe('Component Tests', () => {
  describe('Commessa Management Component', () => {
    let comp: CommessaComponent;
    let fixture: ComponentFixture<CommessaComponent>;
    let service: CommessaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ShoppingonlineTestModule],
        declarations: [CommessaComponent],
      })
        .overrideTemplate(CommessaComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CommessaComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CommessaService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Commessa(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.commessas && comp.commessas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
