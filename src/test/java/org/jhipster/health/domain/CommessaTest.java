package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class CommessaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Commessa.class);
        Commessa commessa1 = new Commessa();
        commessa1.setId(1L);
        Commessa commessa2 = new Commessa();
        commessa2.setId(commessa1.getId());
        assertThat(commessa1).isEqualTo(commessa2);
        commessa2.setId(2L);
        assertThat(commessa1).isNotEqualTo(commessa2);
        commessa1.setId(null);
        assertThat(commessa1).isNotEqualTo(commessa2);
    }
}
