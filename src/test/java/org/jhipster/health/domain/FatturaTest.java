package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class FatturaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fattura.class);
        Fattura fattura1 = new Fattura();
        fattura1.setId(1L);
        Fattura fattura2 = new Fattura();
        fattura2.setId(fattura1.getId());
        assertThat(fattura1).isEqualTo(fattura2);
        fattura2.setId(2L);
        assertThat(fattura1).isNotEqualTo(fattura2);
        fattura1.setId(null);
        assertThat(fattura1).isNotEqualTo(fattura2);
    }
}
