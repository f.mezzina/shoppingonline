package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class ConfigurazioneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Configurazione.class);
        Configurazione configurazione1 = new Configurazione();
        configurazione1.setId(1L);
        Configurazione configurazione2 = new Configurazione();
        configurazione2.setId(configurazione1.getId());
        assertThat(configurazione1).isEqualTo(configurazione2);
        configurazione2.setId(2L);
        assertThat(configurazione1).isNotEqualTo(configurazione2);
        configurazione1.setId(null);
        assertThat(configurazione1).isNotEqualTo(configurazione2);
    }
}
