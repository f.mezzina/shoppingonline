package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class ProfilazioneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Profilazione.class);
        Profilazione profilazione1 = new Profilazione();
        profilazione1.setId(1L);
        Profilazione profilazione2 = new Profilazione();
        profilazione2.setId(profilazione1.getId());
        assertThat(profilazione1).isEqualTo(profilazione2);
        profilazione2.setId(2L);
        assertThat(profilazione1).isNotEqualTo(profilazione2);
        profilazione1.setId(null);
        assertThat(profilazione1).isNotEqualTo(profilazione2);
    }
}
