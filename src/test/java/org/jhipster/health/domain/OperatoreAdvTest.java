package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class OperatoreAdvTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OperatoreAdv.class);
        OperatoreAdv operatoreAdv1 = new OperatoreAdv();
        operatoreAdv1.setId(1L);
        OperatoreAdv operatoreAdv2 = new OperatoreAdv();
        operatoreAdv2.setId(operatoreAdv1.getId());
        assertThat(operatoreAdv1).isEqualTo(operatoreAdv2);
        operatoreAdv2.setId(2L);
        assertThat(operatoreAdv1).isNotEqualTo(operatoreAdv2);
        operatoreAdv1.setId(null);
        assertThat(operatoreAdv1).isNotEqualTo(operatoreAdv2);
    }
}
