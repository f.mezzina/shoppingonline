package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class SpedizioneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Spedizione.class);
        Spedizione spedizione1 = new Spedizione();
        spedizione1.setId(1L);
        Spedizione spedizione2 = new Spedizione();
        spedizione2.setId(spedizione1.getId());
        assertThat(spedizione1).isEqualTo(spedizione2);
        spedizione2.setId(2L);
        assertThat(spedizione1).isNotEqualTo(spedizione2);
        spedizione1.setId(null);
        assertThat(spedizione1).isNotEqualTo(spedizione2);
    }
}
