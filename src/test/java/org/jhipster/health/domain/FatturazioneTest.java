package org.jhipster.health.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.jhipster.health.web.rest.TestUtil;

public class FatturazioneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Fatturazione.class);
        Fatturazione fatturazione1 = new Fatturazione();
        fatturazione1.setId(1L);
        Fatturazione fatturazione2 = new Fatturazione();
        fatturazione2.setId(fatturazione1.getId());
        assertThat(fatturazione1).isEqualTo(fatturazione2);
        fatturazione2.setId(2L);
        assertThat(fatturazione1).isNotEqualTo(fatturazione2);
        fatturazione1.setId(null);
        assertThat(fatturazione1).isNotEqualTo(fatturazione2);
    }
}
