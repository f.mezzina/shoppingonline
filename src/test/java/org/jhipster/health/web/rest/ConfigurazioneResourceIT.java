package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Configurazione;
import org.jhipster.health.repository.ConfigurazioneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ConfigurazioneResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ConfigurazioneResourceIT {

    private static final String DEFAULT_TIPOLOGIA = "AAAAAAAAAA";
    private static final String UPDATED_TIPOLOGIA = "BBBBBBBBBB";

    @Autowired
    private ConfigurazioneRepository configurazioneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restConfigurazioneMockMvc;

    private Configurazione configurazione;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Configurazione createEntity(EntityManager em) {
        Configurazione configurazione = new Configurazione()
            .tipologia(DEFAULT_TIPOLOGIA);
        return configurazione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Configurazione createUpdatedEntity(EntityManager em) {
        Configurazione configurazione = new Configurazione()
            .tipologia(UPDATED_TIPOLOGIA);
        return configurazione;
    }

    @BeforeEach
    public void initTest() {
        configurazione = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfigurazione() throws Exception {
        int databaseSizeBeforeCreate = configurazioneRepository.findAll().size();
        // Create the Configurazione
        restConfigurazioneMockMvc.perform(post("/api/configuraziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(configurazione)))
            .andExpect(status().isCreated());

        // Validate the Configurazione in the database
        List<Configurazione> configurazioneList = configurazioneRepository.findAll();
        assertThat(configurazioneList).hasSize(databaseSizeBeforeCreate + 1);
        Configurazione testConfigurazione = configurazioneList.get(configurazioneList.size() - 1);
        assertThat(testConfigurazione.getTipologia()).isEqualTo(DEFAULT_TIPOLOGIA);
    }

    @Test
    @Transactional
    public void createConfigurazioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = configurazioneRepository.findAll().size();

        // Create the Configurazione with an existing ID
        configurazione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigurazioneMockMvc.perform(post("/api/configuraziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(configurazione)))
            .andExpect(status().isBadRequest());

        // Validate the Configurazione in the database
        List<Configurazione> configurazioneList = configurazioneRepository.findAll();
        assertThat(configurazioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllConfiguraziones() throws Exception {
        // Initialize the database
        configurazioneRepository.saveAndFlush(configurazione);

        // Get all the configurazioneList
        restConfigurazioneMockMvc.perform(get("/api/configuraziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configurazione.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipologia").value(hasItem(DEFAULT_TIPOLOGIA)));
    }
    
    @Test
    @Transactional
    public void getConfigurazione() throws Exception {
        // Initialize the database
        configurazioneRepository.saveAndFlush(configurazione);

        // Get the configurazione
        restConfigurazioneMockMvc.perform(get("/api/configuraziones/{id}", configurazione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(configurazione.getId().intValue()))
            .andExpect(jsonPath("$.tipologia").value(DEFAULT_TIPOLOGIA));
    }
    @Test
    @Transactional
    public void getNonExistingConfigurazione() throws Exception {
        // Get the configurazione
        restConfigurazioneMockMvc.perform(get("/api/configuraziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConfigurazione() throws Exception {
        // Initialize the database
        configurazioneRepository.saveAndFlush(configurazione);

        int databaseSizeBeforeUpdate = configurazioneRepository.findAll().size();

        // Update the configurazione
        Configurazione updatedConfigurazione = configurazioneRepository.findById(configurazione.getId()).get();
        // Disconnect from session so that the updates on updatedConfigurazione are not directly saved in db
        em.detach(updatedConfigurazione);
        updatedConfigurazione
            .tipologia(UPDATED_TIPOLOGIA);

        restConfigurazioneMockMvc.perform(put("/api/configuraziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedConfigurazione)))
            .andExpect(status().isOk());

        // Validate the Configurazione in the database
        List<Configurazione> configurazioneList = configurazioneRepository.findAll();
        assertThat(configurazioneList).hasSize(databaseSizeBeforeUpdate);
        Configurazione testConfigurazione = configurazioneList.get(configurazioneList.size() - 1);
        assertThat(testConfigurazione.getTipologia()).isEqualTo(UPDATED_TIPOLOGIA);
    }

    @Test
    @Transactional
    public void updateNonExistingConfigurazione() throws Exception {
        int databaseSizeBeforeUpdate = configurazioneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConfigurazioneMockMvc.perform(put("/api/configuraziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(configurazione)))
            .andExpect(status().isBadRequest());

        // Validate the Configurazione in the database
        List<Configurazione> configurazioneList = configurazioneRepository.findAll();
        assertThat(configurazioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConfigurazione() throws Exception {
        // Initialize the database
        configurazioneRepository.saveAndFlush(configurazione);

        int databaseSizeBeforeDelete = configurazioneRepository.findAll().size();

        // Delete the configurazione
        restConfigurazioneMockMvc.perform(delete("/api/configuraziones/{id}", configurazione.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Configurazione> configurazioneList = configurazioneRepository.findAll();
        assertThat(configurazioneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
