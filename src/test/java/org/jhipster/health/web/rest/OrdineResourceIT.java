package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Ordine;
import org.jhipster.health.repository.OrdineRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrdineResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrdineResourceIT {

    private static final String DEFAULT_STATO = "AAAAAAAAAA";
    private static final String UPDATED_STATO = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO = 1;
    private static final Integer UPDATED_NUMERO = 2;

    private static final LocalDate DEFAULT_DATA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private OrdineRepository ordineRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrdineMockMvc;

    private Ordine ordine;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ordine createEntity(EntityManager em) {
        Ordine ordine = new Ordine()
            .stato(DEFAULT_STATO)
            .numero(DEFAULT_NUMERO)
            .data(DEFAULT_DATA);
        return ordine;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ordine createUpdatedEntity(EntityManager em) {
        Ordine ordine = new Ordine()
            .stato(UPDATED_STATO)
            .numero(UPDATED_NUMERO)
            .data(UPDATED_DATA);
        return ordine;
    }

    @BeforeEach
    public void initTest() {
        ordine = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrdine() throws Exception {
        int databaseSizeBeforeCreate = ordineRepository.findAll().size();
        // Create the Ordine
        restOrdineMockMvc.perform(post("/api/ordines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordine)))
            .andExpect(status().isCreated());

        // Validate the Ordine in the database
        List<Ordine> ordineList = ordineRepository.findAll();
        assertThat(ordineList).hasSize(databaseSizeBeforeCreate + 1);
        Ordine testOrdine = ordineList.get(ordineList.size() - 1);
        assertThat(testOrdine.getStato()).isEqualTo(DEFAULT_STATO);
        assertThat(testOrdine.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testOrdine.getData()).isEqualTo(DEFAULT_DATA);
    }

    @Test
    @Transactional
    public void createOrdineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ordineRepository.findAll().size();

        // Create the Ordine with an existing ID
        ordine.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrdineMockMvc.perform(post("/api/ordines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordine)))
            .andExpect(status().isBadRequest());

        // Validate the Ordine in the database
        List<Ordine> ordineList = ordineRepository.findAll();
        assertThat(ordineList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrdines() throws Exception {
        // Initialize the database
        ordineRepository.saveAndFlush(ordine);

        // Get all the ordineList
        restOrdineMockMvc.perform(get("/api/ordines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ordine.getId().intValue())))
            .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO)))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())));
    }
    
    @Test
    @Transactional
    public void getOrdine() throws Exception {
        // Initialize the database
        ordineRepository.saveAndFlush(ordine);

        // Get the ordine
        restOrdineMockMvc.perform(get("/api/ordines/{id}", ordine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ordine.getId().intValue()))
            .andExpect(jsonPath("$.stato").value(DEFAULT_STATO))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingOrdine() throws Exception {
        // Get the ordine
        restOrdineMockMvc.perform(get("/api/ordines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrdine() throws Exception {
        // Initialize the database
        ordineRepository.saveAndFlush(ordine);

        int databaseSizeBeforeUpdate = ordineRepository.findAll().size();

        // Update the ordine
        Ordine updatedOrdine = ordineRepository.findById(ordine.getId()).get();
        // Disconnect from session so that the updates on updatedOrdine are not directly saved in db
        em.detach(updatedOrdine);
        updatedOrdine
            .stato(UPDATED_STATO)
            .numero(UPDATED_NUMERO)
            .data(UPDATED_DATA);

        restOrdineMockMvc.perform(put("/api/ordines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrdine)))
            .andExpect(status().isOk());

        // Validate the Ordine in the database
        List<Ordine> ordineList = ordineRepository.findAll();
        assertThat(ordineList).hasSize(databaseSizeBeforeUpdate);
        Ordine testOrdine = ordineList.get(ordineList.size() - 1);
        assertThat(testOrdine.getStato()).isEqualTo(UPDATED_STATO);
        assertThat(testOrdine.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testOrdine.getData()).isEqualTo(UPDATED_DATA);
    }

    @Test
    @Transactional
    public void updateNonExistingOrdine() throws Exception {
        int databaseSizeBeforeUpdate = ordineRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdineMockMvc.perform(put("/api/ordines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ordine)))
            .andExpect(status().isBadRequest());

        // Validate the Ordine in the database
        List<Ordine> ordineList = ordineRepository.findAll();
        assertThat(ordineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrdine() throws Exception {
        // Initialize the database
        ordineRepository.saveAndFlush(ordine);

        int databaseSizeBeforeDelete = ordineRepository.findAll().size();

        // Delete the ordine
        restOrdineMockMvc.perform(delete("/api/ordines/{id}", ordine.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Ordine> ordineList = ordineRepository.findAll();
        assertThat(ordineList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
