package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.OperatoreAdv;
import org.jhipster.health.repository.OperatoreAdvRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OperatoreAdvResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OperatoreAdvResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_COGNOME = "AAAAAAAAAA";
    private static final String UPDATED_COGNOME = "BBBBBBBBBB";

    private static final String DEFAULT_AGENZIA = "AAAAAAAAAA";
    private static final String UPDATED_AGENZIA = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_AUTENTICATO = false;
    private static final Boolean UPDATED_AUTENTICATO = true;

    @Autowired
    private OperatoreAdvRepository operatoreAdvRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOperatoreAdvMockMvc;

    private OperatoreAdv operatoreAdv;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OperatoreAdv createEntity(EntityManager em) {
        OperatoreAdv operatoreAdv = new OperatoreAdv()
            .nome(DEFAULT_NOME)
            .cognome(DEFAULT_COGNOME)
            .agenzia(DEFAULT_AGENZIA)
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .autenticato(DEFAULT_AUTENTICATO);
        return operatoreAdv;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OperatoreAdv createUpdatedEntity(EntityManager em) {
        OperatoreAdv operatoreAdv = new OperatoreAdv()
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .agenzia(UPDATED_AGENZIA)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .autenticato(UPDATED_AUTENTICATO);
        return operatoreAdv;
    }

    @BeforeEach
    public void initTest() {
        operatoreAdv = createEntity(em);
    }

    @Test
    @Transactional
    public void createOperatoreAdv() throws Exception {
        int databaseSizeBeforeCreate = operatoreAdvRepository.findAll().size();
        // Create the OperatoreAdv
        restOperatoreAdvMockMvc.perform(post("/api/operatore-advs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(operatoreAdv)))
            .andExpect(status().isCreated());

        // Validate the OperatoreAdv in the database
        List<OperatoreAdv> operatoreAdvList = operatoreAdvRepository.findAll();
        assertThat(operatoreAdvList).hasSize(databaseSizeBeforeCreate + 1);
        OperatoreAdv testOperatoreAdv = operatoreAdvList.get(operatoreAdvList.size() - 1);
        assertThat(testOperatoreAdv.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testOperatoreAdv.getCognome()).isEqualTo(DEFAULT_COGNOME);
        assertThat(testOperatoreAdv.getAgenzia()).isEqualTo(DEFAULT_AGENZIA);
        assertThat(testOperatoreAdv.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testOperatoreAdv.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testOperatoreAdv.isAutenticato()).isEqualTo(DEFAULT_AUTENTICATO);
    }

    @Test
    @Transactional
    public void createOperatoreAdvWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = operatoreAdvRepository.findAll().size();

        // Create the OperatoreAdv with an existing ID
        operatoreAdv.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOperatoreAdvMockMvc.perform(post("/api/operatore-advs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(operatoreAdv)))
            .andExpect(status().isBadRequest());

        // Validate the OperatoreAdv in the database
        List<OperatoreAdv> operatoreAdvList = operatoreAdvRepository.findAll();
        assertThat(operatoreAdvList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOperatoreAdvs() throws Exception {
        // Initialize the database
        operatoreAdvRepository.saveAndFlush(operatoreAdv);

        // Get all the operatoreAdvList
        restOperatoreAdvMockMvc.perform(get("/api/operatore-advs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(operatoreAdv.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME)))
            .andExpect(jsonPath("$.[*].agenzia").value(hasItem(DEFAULT_AGENZIA)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)))
            .andExpect(jsonPath("$.[*].autenticato").value(hasItem(DEFAULT_AUTENTICATO.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getOperatoreAdv() throws Exception {
        // Initialize the database
        operatoreAdvRepository.saveAndFlush(operatoreAdv);

        // Get the operatoreAdv
        restOperatoreAdvMockMvc.perform(get("/api/operatore-advs/{id}", operatoreAdv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(operatoreAdv.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.cognome").value(DEFAULT_COGNOME))
            .andExpect(jsonPath("$.agenzia").value(DEFAULT_AGENZIA))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD))
            .andExpect(jsonPath("$.autenticato").value(DEFAULT_AUTENTICATO.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingOperatoreAdv() throws Exception {
        // Get the operatoreAdv
        restOperatoreAdvMockMvc.perform(get("/api/operatore-advs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOperatoreAdv() throws Exception {
        // Initialize the database
        operatoreAdvRepository.saveAndFlush(operatoreAdv);

        int databaseSizeBeforeUpdate = operatoreAdvRepository.findAll().size();

        // Update the operatoreAdv
        OperatoreAdv updatedOperatoreAdv = operatoreAdvRepository.findById(operatoreAdv.getId()).get();
        // Disconnect from session so that the updates on updatedOperatoreAdv are not directly saved in db
        em.detach(updatedOperatoreAdv);
        updatedOperatoreAdv
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .agenzia(UPDATED_AGENZIA)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .autenticato(UPDATED_AUTENTICATO);

        restOperatoreAdvMockMvc.perform(put("/api/operatore-advs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOperatoreAdv)))
            .andExpect(status().isOk());

        // Validate the OperatoreAdv in the database
        List<OperatoreAdv> operatoreAdvList = operatoreAdvRepository.findAll();
        assertThat(operatoreAdvList).hasSize(databaseSizeBeforeUpdate);
        OperatoreAdv testOperatoreAdv = operatoreAdvList.get(operatoreAdvList.size() - 1);
        assertThat(testOperatoreAdv.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testOperatoreAdv.getCognome()).isEqualTo(UPDATED_COGNOME);
        assertThat(testOperatoreAdv.getAgenzia()).isEqualTo(UPDATED_AGENZIA);
        assertThat(testOperatoreAdv.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testOperatoreAdv.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testOperatoreAdv.isAutenticato()).isEqualTo(UPDATED_AUTENTICATO);
    }

    @Test
    @Transactional
    public void updateNonExistingOperatoreAdv() throws Exception {
        int databaseSizeBeforeUpdate = operatoreAdvRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOperatoreAdvMockMvc.perform(put("/api/operatore-advs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(operatoreAdv)))
            .andExpect(status().isBadRequest());

        // Validate the OperatoreAdv in the database
        List<OperatoreAdv> operatoreAdvList = operatoreAdvRepository.findAll();
        assertThat(operatoreAdvList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOperatoreAdv() throws Exception {
        // Initialize the database
        operatoreAdvRepository.saveAndFlush(operatoreAdv);

        int databaseSizeBeforeDelete = operatoreAdvRepository.findAll().size();

        // Delete the operatoreAdv
        restOperatoreAdvMockMvc.perform(delete("/api/operatore-advs/{id}", operatoreAdv.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OperatoreAdv> operatoreAdvList = operatoreAdvRepository.findAll();
        assertThat(operatoreAdvList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
