package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Azienda;
import org.jhipster.health.repository.AziendaRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AziendaResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AziendaResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_COGNOME = "AAAAAAAAAA";
    private static final String UPDATED_COGNOME = "BBBBBBBBBB";

    private static final String DEFAULT_AGENZIA = "AAAAAAAAAA";
    private static final String UPDATED_AGENZIA = "BBBBBBBBBB";

    @Autowired
    private AziendaRepository aziendaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAziendaMockMvc;

    private Azienda azienda;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Azienda createEntity(EntityManager em) {
        Azienda azienda = new Azienda()
            .nome(DEFAULT_NOME)
            .cognome(DEFAULT_COGNOME)
            .agenzia(DEFAULT_AGENZIA);
        return azienda;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Azienda createUpdatedEntity(EntityManager em) {
        Azienda azienda = new Azienda()
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .agenzia(UPDATED_AGENZIA);
        return azienda;
    }

    @BeforeEach
    public void initTest() {
        azienda = createEntity(em);
    }

    @Test
    @Transactional
    public void createAzienda() throws Exception {
        int databaseSizeBeforeCreate = aziendaRepository.findAll().size();
        // Create the Azienda
        restAziendaMockMvc.perform(post("/api/aziendas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(azienda)))
            .andExpect(status().isCreated());

        // Validate the Azienda in the database
        List<Azienda> aziendaList = aziendaRepository.findAll();
        assertThat(aziendaList).hasSize(databaseSizeBeforeCreate + 1);
        Azienda testAzienda = aziendaList.get(aziendaList.size() - 1);
        assertThat(testAzienda.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testAzienda.getCognome()).isEqualTo(DEFAULT_COGNOME);
        assertThat(testAzienda.getAgenzia()).isEqualTo(DEFAULT_AGENZIA);
    }

    @Test
    @Transactional
    public void createAziendaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aziendaRepository.findAll().size();

        // Create the Azienda with an existing ID
        azienda.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAziendaMockMvc.perform(post("/api/aziendas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(azienda)))
            .andExpect(status().isBadRequest());

        // Validate the Azienda in the database
        List<Azienda> aziendaList = aziendaRepository.findAll();
        assertThat(aziendaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAziendas() throws Exception {
        // Initialize the database
        aziendaRepository.saveAndFlush(azienda);

        // Get all the aziendaList
        restAziendaMockMvc.perform(get("/api/aziendas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(azienda.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].cognome").value(hasItem(DEFAULT_COGNOME)))
            .andExpect(jsonPath("$.[*].agenzia").value(hasItem(DEFAULT_AGENZIA)));
    }
    
    @Test
    @Transactional
    public void getAzienda() throws Exception {
        // Initialize the database
        aziendaRepository.saveAndFlush(azienda);

        // Get the azienda
        restAziendaMockMvc.perform(get("/api/aziendas/{id}", azienda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(azienda.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.cognome").value(DEFAULT_COGNOME))
            .andExpect(jsonPath("$.agenzia").value(DEFAULT_AGENZIA));
    }
    @Test
    @Transactional
    public void getNonExistingAzienda() throws Exception {
        // Get the azienda
        restAziendaMockMvc.perform(get("/api/aziendas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAzienda() throws Exception {
        // Initialize the database
        aziendaRepository.saveAndFlush(azienda);

        int databaseSizeBeforeUpdate = aziendaRepository.findAll().size();

        // Update the azienda
        Azienda updatedAzienda = aziendaRepository.findById(azienda.getId()).get();
        // Disconnect from session so that the updates on updatedAzienda are not directly saved in db
        em.detach(updatedAzienda);
        updatedAzienda
            .nome(UPDATED_NOME)
            .cognome(UPDATED_COGNOME)
            .agenzia(UPDATED_AGENZIA);

        restAziendaMockMvc.perform(put("/api/aziendas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAzienda)))
            .andExpect(status().isOk());

        // Validate the Azienda in the database
        List<Azienda> aziendaList = aziendaRepository.findAll();
        assertThat(aziendaList).hasSize(databaseSizeBeforeUpdate);
        Azienda testAzienda = aziendaList.get(aziendaList.size() - 1);
        assertThat(testAzienda.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testAzienda.getCognome()).isEqualTo(UPDATED_COGNOME);
        assertThat(testAzienda.getAgenzia()).isEqualTo(UPDATED_AGENZIA);
    }

    @Test
    @Transactional
    public void updateNonExistingAzienda() throws Exception {
        int databaseSizeBeforeUpdate = aziendaRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAziendaMockMvc.perform(put("/api/aziendas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(azienda)))
            .andExpect(status().isBadRequest());

        // Validate the Azienda in the database
        List<Azienda> aziendaList = aziendaRepository.findAll();
        assertThat(aziendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAzienda() throws Exception {
        // Initialize the database
        aziendaRepository.saveAndFlush(azienda);

        int databaseSizeBeforeDelete = aziendaRepository.findAll().size();

        // Delete the azienda
        restAziendaMockMvc.perform(delete("/api/aziendas/{id}", azienda.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Azienda> aziendaList = aziendaRepository.findAll();
        assertThat(aziendaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
