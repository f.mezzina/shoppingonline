package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Spedizione;
import org.jhipster.health.repository.SpedizioneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SpedizioneResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SpedizioneResourceIT {

    private static final String DEFAULT_INDIRIZZO = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO = "BBBBBBBBBB";

    @Autowired
    private SpedizioneRepository spedizioneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSpedizioneMockMvc;

    private Spedizione spedizione;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spedizione createEntity(EntityManager em) {
        Spedizione spedizione = new Spedizione()
            .indirizzo(DEFAULT_INDIRIZZO);
        return spedizione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spedizione createUpdatedEntity(EntityManager em) {
        Spedizione spedizione = new Spedizione()
            .indirizzo(UPDATED_INDIRIZZO);
        return spedizione;
    }

    @BeforeEach
    public void initTest() {
        spedizione = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpedizione() throws Exception {
        int databaseSizeBeforeCreate = spedizioneRepository.findAll().size();
        // Create the Spedizione
        restSpedizioneMockMvc.perform(post("/api/spediziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spedizione)))
            .andExpect(status().isCreated());

        // Validate the Spedizione in the database
        List<Spedizione> spedizioneList = spedizioneRepository.findAll();
        assertThat(spedizioneList).hasSize(databaseSizeBeforeCreate + 1);
        Spedizione testSpedizione = spedizioneList.get(spedizioneList.size() - 1);
        assertThat(testSpedizione.getIndirizzo()).isEqualTo(DEFAULT_INDIRIZZO);
    }

    @Test
    @Transactional
    public void createSpedizioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spedizioneRepository.findAll().size();

        // Create the Spedizione with an existing ID
        spedizione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpedizioneMockMvc.perform(post("/api/spediziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spedizione)))
            .andExpect(status().isBadRequest());

        // Validate the Spedizione in the database
        List<Spedizione> spedizioneList = spedizioneRepository.findAll();
        assertThat(spedizioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSpediziones() throws Exception {
        // Initialize the database
        spedizioneRepository.saveAndFlush(spedizione);

        // Get all the spedizioneList
        restSpedizioneMockMvc.perform(get("/api/spediziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spedizione.getId().intValue())))
            .andExpect(jsonPath("$.[*].indirizzo").value(hasItem(DEFAULT_INDIRIZZO)));
    }
    
    @Test
    @Transactional
    public void getSpedizione() throws Exception {
        // Initialize the database
        spedizioneRepository.saveAndFlush(spedizione);

        // Get the spedizione
        restSpedizioneMockMvc.perform(get("/api/spediziones/{id}", spedizione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(spedizione.getId().intValue()))
            .andExpect(jsonPath("$.indirizzo").value(DEFAULT_INDIRIZZO));
    }
    @Test
    @Transactional
    public void getNonExistingSpedizione() throws Exception {
        // Get the spedizione
        restSpedizioneMockMvc.perform(get("/api/spediziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpedizione() throws Exception {
        // Initialize the database
        spedizioneRepository.saveAndFlush(spedizione);

        int databaseSizeBeforeUpdate = spedizioneRepository.findAll().size();

        // Update the spedizione
        Spedizione updatedSpedizione = spedizioneRepository.findById(spedizione.getId()).get();
        // Disconnect from session so that the updates on updatedSpedizione are not directly saved in db
        em.detach(updatedSpedizione);
        updatedSpedizione
            .indirizzo(UPDATED_INDIRIZZO);

        restSpedizioneMockMvc.perform(put("/api/spediziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSpedizione)))
            .andExpect(status().isOk());

        // Validate the Spedizione in the database
        List<Spedizione> spedizioneList = spedizioneRepository.findAll();
        assertThat(spedizioneList).hasSize(databaseSizeBeforeUpdate);
        Spedizione testSpedizione = spedizioneList.get(spedizioneList.size() - 1);
        assertThat(testSpedizione.getIndirizzo()).isEqualTo(UPDATED_INDIRIZZO);
    }

    @Test
    @Transactional
    public void updateNonExistingSpedizione() throws Exception {
        int databaseSizeBeforeUpdate = spedizioneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpedizioneMockMvc.perform(put("/api/spediziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(spedizione)))
            .andExpect(status().isBadRequest());

        // Validate the Spedizione in the database
        List<Spedizione> spedizioneList = spedizioneRepository.findAll();
        assertThat(spedizioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpedizione() throws Exception {
        // Initialize the database
        spedizioneRepository.saveAndFlush(spedizione);

        int databaseSizeBeforeDelete = spedizioneRepository.findAll().size();

        // Delete the spedizione
        restSpedizioneMockMvc.perform(delete("/api/spediziones/{id}", spedizione.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Spedizione> spedizioneList = spedizioneRepository.findAll();
        assertThat(spedizioneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
