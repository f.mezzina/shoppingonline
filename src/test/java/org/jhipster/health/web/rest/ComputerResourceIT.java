package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Computer;
import org.jhipster.health.repository.ComputerRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ComputerResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ComputerResourceIT {

    private static final BigDecimal DEFAULT_PREZZO = new BigDecimal(1);
    private static final BigDecimal UPDATED_PREZZO = new BigDecimal(2);

    @Autowired
    private ComputerRepository computerRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restComputerMockMvc;

    private Computer computer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Computer createEntity(EntityManager em) {
        Computer computer = new Computer()
            .prezzo(DEFAULT_PREZZO);
        return computer;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Computer createUpdatedEntity(EntityManager em) {
        Computer computer = new Computer()
            .prezzo(UPDATED_PREZZO);
        return computer;
    }

    @BeforeEach
    public void initTest() {
        computer = createEntity(em);
    }

    @Test
    @Transactional
    public void createComputer() throws Exception {
        int databaseSizeBeforeCreate = computerRepository.findAll().size();
        // Create the Computer
        restComputerMockMvc.perform(post("/api/computers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(computer)))
            .andExpect(status().isCreated());

        // Validate the Computer in the database
        List<Computer> computerList = computerRepository.findAll();
        assertThat(computerList).hasSize(databaseSizeBeforeCreate + 1);
        Computer testComputer = computerList.get(computerList.size() - 1);
        assertThat(testComputer.getPrezzo()).isEqualTo(DEFAULT_PREZZO);
    }

    @Test
    @Transactional
    public void createComputerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = computerRepository.findAll().size();

        // Create the Computer with an existing ID
        computer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restComputerMockMvc.perform(post("/api/computers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(computer)))
            .andExpect(status().isBadRequest());

        // Validate the Computer in the database
        List<Computer> computerList = computerRepository.findAll();
        assertThat(computerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllComputers() throws Exception {
        // Initialize the database
        computerRepository.saveAndFlush(computer);

        // Get all the computerList
        restComputerMockMvc.perform(get("/api/computers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(computer.getId().intValue())))
            .andExpect(jsonPath("$.[*].prezzo").value(hasItem(DEFAULT_PREZZO.intValue())));
    }
    
    @Test
    @Transactional
    public void getComputer() throws Exception {
        // Initialize the database
        computerRepository.saveAndFlush(computer);

        // Get the computer
        restComputerMockMvc.perform(get("/api/computers/{id}", computer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(computer.getId().intValue()))
            .andExpect(jsonPath("$.prezzo").value(DEFAULT_PREZZO.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingComputer() throws Exception {
        // Get the computer
        restComputerMockMvc.perform(get("/api/computers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComputer() throws Exception {
        // Initialize the database
        computerRepository.saveAndFlush(computer);

        int databaseSizeBeforeUpdate = computerRepository.findAll().size();

        // Update the computer
        Computer updatedComputer = computerRepository.findById(computer.getId()).get();
        // Disconnect from session so that the updates on updatedComputer are not directly saved in db
        em.detach(updatedComputer);
        updatedComputer
            .prezzo(UPDATED_PREZZO);

        restComputerMockMvc.perform(put("/api/computers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedComputer)))
            .andExpect(status().isOk());

        // Validate the Computer in the database
        List<Computer> computerList = computerRepository.findAll();
        assertThat(computerList).hasSize(databaseSizeBeforeUpdate);
        Computer testComputer = computerList.get(computerList.size() - 1);
        assertThat(testComputer.getPrezzo()).isEqualTo(UPDATED_PREZZO);
    }

    @Test
    @Transactional
    public void updateNonExistingComputer() throws Exception {
        int databaseSizeBeforeUpdate = computerRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restComputerMockMvc.perform(put("/api/computers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(computer)))
            .andExpect(status().isBadRequest());

        // Validate the Computer in the database
        List<Computer> computerList = computerRepository.findAll();
        assertThat(computerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteComputer() throws Exception {
        // Initialize the database
        computerRepository.saveAndFlush(computer);

        int databaseSizeBeforeDelete = computerRepository.findAll().size();

        // Delete the computer
        restComputerMockMvc.perform(delete("/api/computers/{id}", computer.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Computer> computerList = computerRepository.findAll();
        assertThat(computerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
