package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Catalogo;
import org.jhipster.health.repository.CatalogoRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CatalogoResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CatalogoResourceIT {

    @Autowired
    private CatalogoRepository catalogoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCatalogoMockMvc;

    private Catalogo catalogo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Catalogo createEntity(EntityManager em) {
        Catalogo catalogo = new Catalogo();
        return catalogo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Catalogo createUpdatedEntity(EntityManager em) {
        Catalogo catalogo = new Catalogo();
        return catalogo;
    }

    @BeforeEach
    public void initTest() {
        catalogo = createEntity(em);
    }

    @Test
    @Transactional
    public void createCatalogo() throws Exception {
        int databaseSizeBeforeCreate = catalogoRepository.findAll().size();
        // Create the Catalogo
        restCatalogoMockMvc.perform(post("/api/catalogos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(catalogo)))
            .andExpect(status().isCreated());

        // Validate the Catalogo in the database
        List<Catalogo> catalogoList = catalogoRepository.findAll();
        assertThat(catalogoList).hasSize(databaseSizeBeforeCreate + 1);
        Catalogo testCatalogo = catalogoList.get(catalogoList.size() - 1);
    }

    @Test
    @Transactional
    public void createCatalogoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = catalogoRepository.findAll().size();

        // Create the Catalogo with an existing ID
        catalogo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCatalogoMockMvc.perform(post("/api/catalogos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(catalogo)))
            .andExpect(status().isBadRequest());

        // Validate the Catalogo in the database
        List<Catalogo> catalogoList = catalogoRepository.findAll();
        assertThat(catalogoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCatalogos() throws Exception {
        // Initialize the database
        catalogoRepository.saveAndFlush(catalogo);

        // Get all the catalogoList
        restCatalogoMockMvc.perform(get("/api/catalogos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(catalogo.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getCatalogo() throws Exception {
        // Initialize the database
        catalogoRepository.saveAndFlush(catalogo);

        // Get the catalogo
        restCatalogoMockMvc.perform(get("/api/catalogos/{id}", catalogo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(catalogo.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCatalogo() throws Exception {
        // Get the catalogo
        restCatalogoMockMvc.perform(get("/api/catalogos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCatalogo() throws Exception {
        // Initialize the database
        catalogoRepository.saveAndFlush(catalogo);

        int databaseSizeBeforeUpdate = catalogoRepository.findAll().size();

        // Update the catalogo
        Catalogo updatedCatalogo = catalogoRepository.findById(catalogo.getId()).get();
        // Disconnect from session so that the updates on updatedCatalogo are not directly saved in db
        em.detach(updatedCatalogo);

        restCatalogoMockMvc.perform(put("/api/catalogos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCatalogo)))
            .andExpect(status().isOk());

        // Validate the Catalogo in the database
        List<Catalogo> catalogoList = catalogoRepository.findAll();
        assertThat(catalogoList).hasSize(databaseSizeBeforeUpdate);
        Catalogo testCatalogo = catalogoList.get(catalogoList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCatalogo() throws Exception {
        int databaseSizeBeforeUpdate = catalogoRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCatalogoMockMvc.perform(put("/api/catalogos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(catalogo)))
            .andExpect(status().isBadRequest());

        // Validate the Catalogo in the database
        List<Catalogo> catalogoList = catalogoRepository.findAll();
        assertThat(catalogoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCatalogo() throws Exception {
        // Initialize the database
        catalogoRepository.saveAndFlush(catalogo);

        int databaseSizeBeforeDelete = catalogoRepository.findAll().size();

        // Delete the catalogo
        restCatalogoMockMvc.perform(delete("/api/catalogos/{id}", catalogo.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Catalogo> catalogoList = catalogoRepository.findAll();
        assertThat(catalogoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
