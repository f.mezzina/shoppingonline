package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Fattura;
import org.jhipster.health.repository.FatturaRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FatturaResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FatturaResourceIT {

    private static final String DEFAULT_PARTITA_IVA = "AAAAAAAAAA";
    private static final String UPDATED_PARTITA_IVA = "BBBBBBBBBB";

    private static final String DEFAULT_RAGIONE_SOCIALE = "AAAAAAAAAA";
    private static final String UPDATED_RAGIONE_SOCIALE = "BBBBBBBBBB";

    @Autowired
    private FatturaRepository fatturaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFatturaMockMvc;

    private Fattura fattura;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fattura createEntity(EntityManager em) {
        Fattura fattura = new Fattura()
            .partitaIva(DEFAULT_PARTITA_IVA)
            .ragioneSociale(DEFAULT_RAGIONE_SOCIALE);
        return fattura;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fattura createUpdatedEntity(EntityManager em) {
        Fattura fattura = new Fattura()
            .partitaIva(UPDATED_PARTITA_IVA)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE);
        return fattura;
    }

    @BeforeEach
    public void initTest() {
        fattura = createEntity(em);
    }

    @Test
    @Transactional
    public void createFattura() throws Exception {
        int databaseSizeBeforeCreate = fatturaRepository.findAll().size();
        // Create the Fattura
        restFatturaMockMvc.perform(post("/api/fatturas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fattura)))
            .andExpect(status().isCreated());

        // Validate the Fattura in the database
        List<Fattura> fatturaList = fatturaRepository.findAll();
        assertThat(fatturaList).hasSize(databaseSizeBeforeCreate + 1);
        Fattura testFattura = fatturaList.get(fatturaList.size() - 1);
        assertThat(testFattura.getPartitaIva()).isEqualTo(DEFAULT_PARTITA_IVA);
        assertThat(testFattura.getRagioneSociale()).isEqualTo(DEFAULT_RAGIONE_SOCIALE);
    }

    @Test
    @Transactional
    public void createFatturaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fatturaRepository.findAll().size();

        // Create the Fattura with an existing ID
        fattura.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFatturaMockMvc.perform(post("/api/fatturas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fattura)))
            .andExpect(status().isBadRequest());

        // Validate the Fattura in the database
        List<Fattura> fatturaList = fatturaRepository.findAll();
        assertThat(fatturaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFatturas() throws Exception {
        // Initialize the database
        fatturaRepository.saveAndFlush(fattura);

        // Get all the fatturaList
        restFatturaMockMvc.perform(get("/api/fatturas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fattura.getId().intValue())))
            .andExpect(jsonPath("$.[*].partitaIva").value(hasItem(DEFAULT_PARTITA_IVA)))
            .andExpect(jsonPath("$.[*].ragioneSociale").value(hasItem(DEFAULT_RAGIONE_SOCIALE)));
    }
    
    @Test
    @Transactional
    public void getFattura() throws Exception {
        // Initialize the database
        fatturaRepository.saveAndFlush(fattura);

        // Get the fattura
        restFatturaMockMvc.perform(get("/api/fatturas/{id}", fattura.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fattura.getId().intValue()))
            .andExpect(jsonPath("$.partitaIva").value(DEFAULT_PARTITA_IVA))
            .andExpect(jsonPath("$.ragioneSociale").value(DEFAULT_RAGIONE_SOCIALE));
    }
    @Test
    @Transactional
    public void getNonExistingFattura() throws Exception {
        // Get the fattura
        restFatturaMockMvc.perform(get("/api/fatturas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFattura() throws Exception {
        // Initialize the database
        fatturaRepository.saveAndFlush(fattura);

        int databaseSizeBeforeUpdate = fatturaRepository.findAll().size();

        // Update the fattura
        Fattura updatedFattura = fatturaRepository.findById(fattura.getId()).get();
        // Disconnect from session so that the updates on updatedFattura are not directly saved in db
        em.detach(updatedFattura);
        updatedFattura
            .partitaIva(UPDATED_PARTITA_IVA)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE);

        restFatturaMockMvc.perform(put("/api/fatturas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFattura)))
            .andExpect(status().isOk());

        // Validate the Fattura in the database
        List<Fattura> fatturaList = fatturaRepository.findAll();
        assertThat(fatturaList).hasSize(databaseSizeBeforeUpdate);
        Fattura testFattura = fatturaList.get(fatturaList.size() - 1);
        assertThat(testFattura.getPartitaIva()).isEqualTo(UPDATED_PARTITA_IVA);
        assertThat(testFattura.getRagioneSociale()).isEqualTo(UPDATED_RAGIONE_SOCIALE);
    }

    @Test
    @Transactional
    public void updateNonExistingFattura() throws Exception {
        int databaseSizeBeforeUpdate = fatturaRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFatturaMockMvc.perform(put("/api/fatturas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fattura)))
            .andExpect(status().isBadRequest());

        // Validate the Fattura in the database
        List<Fattura> fatturaList = fatturaRepository.findAll();
        assertThat(fatturaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFattura() throws Exception {
        // Initialize the database
        fatturaRepository.saveAndFlush(fattura);

        int databaseSizeBeforeDelete = fatturaRepository.findAll().size();

        // Delete the fattura
        restFatturaMockMvc.perform(delete("/api/fatturas/{id}", fattura.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fattura> fatturaList = fatturaRepository.findAll();
        assertThat(fatturaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
