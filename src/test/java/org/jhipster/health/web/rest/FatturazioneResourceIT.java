package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Fatturazione;
import org.jhipster.health.repository.FatturazioneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FatturazioneResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FatturazioneResourceIT {

    @Autowired
    private FatturazioneRepository fatturazioneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFatturazioneMockMvc;

    private Fatturazione fatturazione;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fatturazione createEntity(EntityManager em) {
        Fatturazione fatturazione = new Fatturazione();
        return fatturazione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fatturazione createUpdatedEntity(EntityManager em) {
        Fatturazione fatturazione = new Fatturazione();
        return fatturazione;
    }

    @BeforeEach
    public void initTest() {
        fatturazione = createEntity(em);
    }

    @Test
    @Transactional
    public void createFatturazione() throws Exception {
        int databaseSizeBeforeCreate = fatturazioneRepository.findAll().size();
        // Create the Fatturazione
        restFatturazioneMockMvc.perform(post("/api/fatturaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fatturazione)))
            .andExpect(status().isCreated());

        // Validate the Fatturazione in the database
        List<Fatturazione> fatturazioneList = fatturazioneRepository.findAll();
        assertThat(fatturazioneList).hasSize(databaseSizeBeforeCreate + 1);
        Fatturazione testFatturazione = fatturazioneList.get(fatturazioneList.size() - 1);
    }

    @Test
    @Transactional
    public void createFatturazioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fatturazioneRepository.findAll().size();

        // Create the Fatturazione with an existing ID
        fatturazione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFatturazioneMockMvc.perform(post("/api/fatturaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fatturazione)))
            .andExpect(status().isBadRequest());

        // Validate the Fatturazione in the database
        List<Fatturazione> fatturazioneList = fatturazioneRepository.findAll();
        assertThat(fatturazioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFatturaziones() throws Exception {
        // Initialize the database
        fatturazioneRepository.saveAndFlush(fatturazione);

        // Get all the fatturazioneList
        restFatturazioneMockMvc.perform(get("/api/fatturaziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fatturazione.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getFatturazione() throws Exception {
        // Initialize the database
        fatturazioneRepository.saveAndFlush(fatturazione);

        // Get the fatturazione
        restFatturazioneMockMvc.perform(get("/api/fatturaziones/{id}", fatturazione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fatturazione.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFatturazione() throws Exception {
        // Get the fatturazione
        restFatturazioneMockMvc.perform(get("/api/fatturaziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFatturazione() throws Exception {
        // Initialize the database
        fatturazioneRepository.saveAndFlush(fatturazione);

        int databaseSizeBeforeUpdate = fatturazioneRepository.findAll().size();

        // Update the fatturazione
        Fatturazione updatedFatturazione = fatturazioneRepository.findById(fatturazione.getId()).get();
        // Disconnect from session so that the updates on updatedFatturazione are not directly saved in db
        em.detach(updatedFatturazione);

        restFatturazioneMockMvc.perform(put("/api/fatturaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFatturazione)))
            .andExpect(status().isOk());

        // Validate the Fatturazione in the database
        List<Fatturazione> fatturazioneList = fatturazioneRepository.findAll();
        assertThat(fatturazioneList).hasSize(databaseSizeBeforeUpdate);
        Fatturazione testFatturazione = fatturazioneList.get(fatturazioneList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingFatturazione() throws Exception {
        int databaseSizeBeforeUpdate = fatturazioneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFatturazioneMockMvc.perform(put("/api/fatturaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fatturazione)))
            .andExpect(status().isBadRequest());

        // Validate the Fatturazione in the database
        List<Fatturazione> fatturazioneList = fatturazioneRepository.findAll();
        assertThat(fatturazioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFatturazione() throws Exception {
        // Initialize the database
        fatturazioneRepository.saveAndFlush(fatturazione);

        int databaseSizeBeforeDelete = fatturazioneRepository.findAll().size();

        // Delete the fatturazione
        restFatturazioneMockMvc.perform(delete("/api/fatturaziones/{id}", fatturazione.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fatturazione> fatturazioneList = fatturazioneRepository.findAll();
        assertThat(fatturazioneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
