package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Profilazione;
import org.jhipster.health.repository.ProfilazioneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProfilazioneResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProfilazioneResourceIT {

    @Autowired
    private ProfilazioneRepository profilazioneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProfilazioneMockMvc;

    private Profilazione profilazione;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Profilazione createEntity(EntityManager em) {
        Profilazione profilazione = new Profilazione();
        return profilazione;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Profilazione createUpdatedEntity(EntityManager em) {
        Profilazione profilazione = new Profilazione();
        return profilazione;
    }

    @BeforeEach
    public void initTest() {
        profilazione = createEntity(em);
    }

    @Test
    @Transactional
    public void createProfilazione() throws Exception {
        int databaseSizeBeforeCreate = profilazioneRepository.findAll().size();
        // Create the Profilazione
        restProfilazioneMockMvc.perform(post("/api/profilaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilazione)))
            .andExpect(status().isCreated());

        // Validate the Profilazione in the database
        List<Profilazione> profilazioneList = profilazioneRepository.findAll();
        assertThat(profilazioneList).hasSize(databaseSizeBeforeCreate + 1);
        Profilazione testProfilazione = profilazioneList.get(profilazioneList.size() - 1);
    }

    @Test
    @Transactional
    public void createProfilazioneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = profilazioneRepository.findAll().size();

        // Create the Profilazione with an existing ID
        profilazione.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProfilazioneMockMvc.perform(post("/api/profilaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilazione)))
            .andExpect(status().isBadRequest());

        // Validate the Profilazione in the database
        List<Profilazione> profilazioneList = profilazioneRepository.findAll();
        assertThat(profilazioneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProfilaziones() throws Exception {
        // Initialize the database
        profilazioneRepository.saveAndFlush(profilazione);

        // Get all the profilazioneList
        restProfilazioneMockMvc.perform(get("/api/profilaziones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profilazione.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getProfilazione() throws Exception {
        // Initialize the database
        profilazioneRepository.saveAndFlush(profilazione);

        // Get the profilazione
        restProfilazioneMockMvc.perform(get("/api/profilaziones/{id}", profilazione.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(profilazione.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProfilazione() throws Exception {
        // Get the profilazione
        restProfilazioneMockMvc.perform(get("/api/profilaziones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProfilazione() throws Exception {
        // Initialize the database
        profilazioneRepository.saveAndFlush(profilazione);

        int databaseSizeBeforeUpdate = profilazioneRepository.findAll().size();

        // Update the profilazione
        Profilazione updatedProfilazione = profilazioneRepository.findById(profilazione.getId()).get();
        // Disconnect from session so that the updates on updatedProfilazione are not directly saved in db
        em.detach(updatedProfilazione);

        restProfilazioneMockMvc.perform(put("/api/profilaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProfilazione)))
            .andExpect(status().isOk());

        // Validate the Profilazione in the database
        List<Profilazione> profilazioneList = profilazioneRepository.findAll();
        assertThat(profilazioneList).hasSize(databaseSizeBeforeUpdate);
        Profilazione testProfilazione = profilazioneList.get(profilazioneList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingProfilazione() throws Exception {
        int databaseSizeBeforeUpdate = profilazioneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProfilazioneMockMvc.perform(put("/api/profilaziones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profilazione)))
            .andExpect(status().isBadRequest());

        // Validate the Profilazione in the database
        List<Profilazione> profilazioneList = profilazioneRepository.findAll();
        assertThat(profilazioneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProfilazione() throws Exception {
        // Initialize the database
        profilazioneRepository.saveAndFlush(profilazione);

        int databaseSizeBeforeDelete = profilazioneRepository.findAll().size();

        // Delete the profilazione
        restProfilazioneMockMvc.perform(delete("/api/profilaziones/{id}", profilazione.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Profilazione> profilazioneList = profilazioneRepository.findAll();
        assertThat(profilazioneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
