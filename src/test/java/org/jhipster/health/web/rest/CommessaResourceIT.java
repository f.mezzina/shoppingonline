package org.jhipster.health.web.rest;

import org.jhipster.health.ShoppingonlineApp;
import org.jhipster.health.domain.Commessa;
import org.jhipster.health.repository.CommessaRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CommessaResource} REST controller.
 */
@SpringBootTest(classes = ShoppingonlineApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CommessaResourceIT {

    @Autowired
    private CommessaRepository commessaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommessaMockMvc;

    private Commessa commessa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Commessa createEntity(EntityManager em) {
        Commessa commessa = new Commessa();
        return commessa;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Commessa createUpdatedEntity(EntityManager em) {
        Commessa commessa = new Commessa();
        return commessa;
    }

    @BeforeEach
    public void initTest() {
        commessa = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommessa() throws Exception {
        int databaseSizeBeforeCreate = commessaRepository.findAll().size();
        // Create the Commessa
        restCommessaMockMvc.perform(post("/api/commessas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commessa)))
            .andExpect(status().isCreated());

        // Validate the Commessa in the database
        List<Commessa> commessaList = commessaRepository.findAll();
        assertThat(commessaList).hasSize(databaseSizeBeforeCreate + 1);
        Commessa testCommessa = commessaList.get(commessaList.size() - 1);
    }

    @Test
    @Transactional
    public void createCommessaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commessaRepository.findAll().size();

        // Create the Commessa with an existing ID
        commessa.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommessaMockMvc.perform(post("/api/commessas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commessa)))
            .andExpect(status().isBadRequest());

        // Validate the Commessa in the database
        List<Commessa> commessaList = commessaRepository.findAll();
        assertThat(commessaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCommessas() throws Exception {
        // Initialize the database
        commessaRepository.saveAndFlush(commessa);

        // Get all the commessaList
        restCommessaMockMvc.perform(get("/api/commessas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commessa.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getCommessa() throws Exception {
        // Initialize the database
        commessaRepository.saveAndFlush(commessa);

        // Get the commessa
        restCommessaMockMvc.perform(get("/api/commessas/{id}", commessa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commessa.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCommessa() throws Exception {
        // Get the commessa
        restCommessaMockMvc.perform(get("/api/commessas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommessa() throws Exception {
        // Initialize the database
        commessaRepository.saveAndFlush(commessa);

        int databaseSizeBeforeUpdate = commessaRepository.findAll().size();

        // Update the commessa
        Commessa updatedCommessa = commessaRepository.findById(commessa.getId()).get();
        // Disconnect from session so that the updates on updatedCommessa are not directly saved in db
        em.detach(updatedCommessa);

        restCommessaMockMvc.perform(put("/api/commessas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCommessa)))
            .andExpect(status().isOk());

        // Validate the Commessa in the database
        List<Commessa> commessaList = commessaRepository.findAll();
        assertThat(commessaList).hasSize(databaseSizeBeforeUpdate);
        Commessa testCommessa = commessaList.get(commessaList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCommessa() throws Exception {
        int databaseSizeBeforeUpdate = commessaRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommessaMockMvc.perform(put("/api/commessas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(commessa)))
            .andExpect(status().isBadRequest());

        // Validate the Commessa in the database
        List<Commessa> commessaList = commessaRepository.findAll();
        assertThat(commessaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCommessa() throws Exception {
        // Initialize the database
        commessaRepository.saveAndFlush(commessa);

        int databaseSizeBeforeDelete = commessaRepository.findAll().size();

        // Delete the commessa
        restCommessaMockMvc.perform(delete("/api/commessas/{id}", commessa.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Commessa> commessaList = commessaRepository.findAll();
        assertThat(commessaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
