package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Utente;
import org.jhipster.health.repository.UtenteRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Utente}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class UtenteResource {

    private final Logger log = LoggerFactory.getLogger(UtenteResource.class);

    private static final String ENTITY_NAME = "utente";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UtenteRepository utenteRepository;

    public UtenteResource(UtenteRepository utenteRepository) {
        this.utenteRepository = utenteRepository;
    }

    /**
     * {@code POST  /utentes} : Create a new utente.
     *
     * @param utente the utente to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new utente, or with status {@code 400 (Bad Request)} if the utente has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/utentes")
    public ResponseEntity<Utente> createUtente(@RequestBody Utente utente) throws URISyntaxException {
        log.debug("REST request to save Utente : {}", utente);
        if (utente.getId() != null) {
            throw new BadRequestAlertException("A new utente cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Utente result = utenteRepository.save(utente);
        return ResponseEntity.created(new URI("/api/utentes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /utentes} : Updates an existing utente.
     *
     * @param utente the utente to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated utente,
     * or with status {@code 400 (Bad Request)} if the utente is not valid,
     * or with status {@code 500 (Internal Server Error)} if the utente couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/utentes")
    public ResponseEntity<Utente> updateUtente(@RequestBody Utente utente) throws URISyntaxException {
        log.debug("REST request to update Utente : {}", utente);
        if (utente.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Utente result = utenteRepository.save(utente);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, utente.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /utentes} : get all the utentes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of utentes in body.
     */
    @GetMapping("/utentes")
    public List<Utente> getAllUtentes() {
        log.debug("REST request to get all Utentes");
        return utenteRepository.findAll();
    }

    /**
     * {@code GET  /utentes/:id} : get the "id" utente.
     *
     * @param id the id of the utente to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the utente, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/utentes/{id}")
    public ResponseEntity<Utente> getUtente(@PathVariable Long id) {
        log.debug("REST request to get Utente : {}", id);
        Optional<Utente> utente = utenteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(utente);
    }

    /**
     * {@code DELETE  /utentes/:id} : delete the "id" utente.
     *
     * @param id the id of the utente to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/utentes/{id}")
    public ResponseEntity<Void> deleteUtente(@PathVariable Long id) {
        log.debug("REST request to delete Utente : {}", id);
        utenteRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
