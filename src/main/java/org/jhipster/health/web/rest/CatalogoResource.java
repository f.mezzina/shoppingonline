package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Catalogo;
import org.jhipster.health.repository.CatalogoRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Catalogo}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CatalogoResource {

    private final Logger log = LoggerFactory.getLogger(CatalogoResource.class);

    private static final String ENTITY_NAME = "catalogo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CatalogoRepository catalogoRepository;

    public CatalogoResource(CatalogoRepository catalogoRepository) {
        this.catalogoRepository = catalogoRepository;
    }

    /**
     * {@code POST  /catalogos} : Create a new catalogo.
     *
     * @param catalogo the catalogo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new catalogo, or with status {@code 400 (Bad Request)} if the catalogo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/catalogos")
    public ResponseEntity<Catalogo> createCatalogo(@RequestBody Catalogo catalogo) throws URISyntaxException {
        log.debug("REST request to save Catalogo : {}", catalogo);
        if (catalogo.getId() != null) {
            throw new BadRequestAlertException("A new catalogo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Catalogo result = catalogoRepository.save(catalogo);
        return ResponseEntity.created(new URI("/api/catalogos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /catalogos} : Updates an existing catalogo.
     *
     * @param catalogo the catalogo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated catalogo,
     * or with status {@code 400 (Bad Request)} if the catalogo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the catalogo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/catalogos")
    public ResponseEntity<Catalogo> updateCatalogo(@RequestBody Catalogo catalogo) throws URISyntaxException {
        log.debug("REST request to update Catalogo : {}", catalogo);
        if (catalogo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Catalogo result = catalogoRepository.save(catalogo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, catalogo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /catalogos} : get all the catalogos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of catalogos in body.
     */
    @GetMapping("/catalogos")
    public List<Catalogo> getAllCatalogos() {
        log.debug("REST request to get all Catalogos");
        return catalogoRepository.findAll();
    }

    /**
     * {@code GET  /catalogos/:id} : get the "id" catalogo.
     *
     * @param id the id of the catalogo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the catalogo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/catalogos/{id}")
    public ResponseEntity<Catalogo> getCatalogo(@PathVariable Long id) {
        log.debug("REST request to get Catalogo : {}", id);
        Optional<Catalogo> catalogo = catalogoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(catalogo);
    }

    /**
     * {@code DELETE  /catalogos/:id} : delete the "id" catalogo.
     *
     * @param id the id of the catalogo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/catalogos/{id}")
    public ResponseEntity<Void> deleteCatalogo(@PathVariable Long id) {
        log.debug("REST request to delete Catalogo : {}", id);
        catalogoRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
