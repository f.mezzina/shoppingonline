package org.jhipster.health.web.rest;

import org.jhipster.health.domain.OperatoreAdv;
import org.jhipster.health.repository.OperatoreAdvRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.OperatoreAdv}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class OperatoreAdvResource {

    private final Logger log = LoggerFactory.getLogger(OperatoreAdvResource.class);

    private static final String ENTITY_NAME = "operatoreAdv";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OperatoreAdvRepository operatoreAdvRepository;

    public OperatoreAdvResource(OperatoreAdvRepository operatoreAdvRepository) {
        this.operatoreAdvRepository = operatoreAdvRepository;
    }

    /**
     * {@code POST  /operatore-advs} : Create a new operatoreAdv.
     *
     * @param operatoreAdv the operatoreAdv to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new operatoreAdv, or with status {@code 400 (Bad Request)} if the operatoreAdv has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/operatore-advs")
    public ResponseEntity<OperatoreAdv> createOperatoreAdv(@RequestBody OperatoreAdv operatoreAdv) throws URISyntaxException {
        log.debug("REST request to save OperatoreAdv : {}", operatoreAdv);
        if (operatoreAdv.getId() != null) {
            throw new BadRequestAlertException("A new operatoreAdv cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OperatoreAdv result = operatoreAdvRepository.save(operatoreAdv);
        return ResponseEntity.created(new URI("/api/operatore-advs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /operatore-advs} : Updates an existing operatoreAdv.
     *
     * @param operatoreAdv the operatoreAdv to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated operatoreAdv,
     * or with status {@code 400 (Bad Request)} if the operatoreAdv is not valid,
     * or with status {@code 500 (Internal Server Error)} if the operatoreAdv couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/operatore-advs")
    public ResponseEntity<OperatoreAdv> updateOperatoreAdv(@RequestBody OperatoreAdv operatoreAdv) throws URISyntaxException {
        log.debug("REST request to update OperatoreAdv : {}", operatoreAdv);
        if (operatoreAdv.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OperatoreAdv result = operatoreAdvRepository.save(operatoreAdv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, operatoreAdv.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /operatore-advs} : get all the operatoreAdvs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of operatoreAdvs in body.
     */
    @GetMapping("/operatore-advs")
    public List<OperatoreAdv> getAllOperatoreAdvs() {
        log.debug("REST request to get all OperatoreAdvs");
        return operatoreAdvRepository.findAll();
    }

    /**
     * {@code GET  /operatore-advs/:id} : get the "id" operatoreAdv.
     *
     * @param id the id of the operatoreAdv to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the operatoreAdv, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/operatore-advs/{id}")
    public ResponseEntity<OperatoreAdv> getOperatoreAdv(@PathVariable Long id) {
        log.debug("REST request to get OperatoreAdv : {}", id);
        Optional<OperatoreAdv> operatoreAdv = operatoreAdvRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(operatoreAdv);
    }

    /**
     * {@code DELETE  /operatore-advs/:id} : delete the "id" operatoreAdv.
     *
     * @param id the id of the operatoreAdv to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/operatore-advs/{id}")
    public ResponseEntity<Void> deleteOperatoreAdv(@PathVariable Long id) {
        log.debug("REST request to delete OperatoreAdv : {}", id);
        operatoreAdvRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
