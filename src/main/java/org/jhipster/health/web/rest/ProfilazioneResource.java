package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Profilazione;
import org.jhipster.health.repository.ProfilazioneRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Profilazione}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProfilazioneResource {

    private final Logger log = LoggerFactory.getLogger(ProfilazioneResource.class);

    private static final String ENTITY_NAME = "profilazione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProfilazioneRepository profilazioneRepository;

    public ProfilazioneResource(ProfilazioneRepository profilazioneRepository) {
        this.profilazioneRepository = profilazioneRepository;
    }

    /**
     * {@code POST  /profilaziones} : Create a new profilazione.
     *
     * @param profilazione the profilazione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new profilazione, or with status {@code 400 (Bad Request)} if the profilazione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/profilaziones")
    public ResponseEntity<Profilazione> createProfilazione(@RequestBody Profilazione profilazione) throws URISyntaxException {
        log.debug("REST request to save Profilazione : {}", profilazione);
        if (profilazione.getId() != null) {
            throw new BadRequestAlertException("A new profilazione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Profilazione result = profilazioneRepository.save(profilazione);
        return ResponseEntity.created(new URI("/api/profilaziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /profilaziones} : Updates an existing profilazione.
     *
     * @param profilazione the profilazione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated profilazione,
     * or with status {@code 400 (Bad Request)} if the profilazione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the profilazione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/profilaziones")
    public ResponseEntity<Profilazione> updateProfilazione(@RequestBody Profilazione profilazione) throws URISyntaxException {
        log.debug("REST request to update Profilazione : {}", profilazione);
        if (profilazione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Profilazione result = profilazioneRepository.save(profilazione);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, profilazione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /profilaziones} : get all the profilaziones.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of profilaziones in body.
     */
    @GetMapping("/profilaziones")
    public List<Profilazione> getAllProfilaziones() {
        log.debug("REST request to get all Profilaziones");
        return profilazioneRepository.findAll();
    }

    /**
     * {@code GET  /profilaziones/:id} : get the "id" profilazione.
     *
     * @param id the id of the profilazione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the profilazione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/profilaziones/{id}")
    public ResponseEntity<Profilazione> getProfilazione(@PathVariable Long id) {
        log.debug("REST request to get Profilazione : {}", id);
        Optional<Profilazione> profilazione = profilazioneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(profilazione);
    }

    /**
     * {@code DELETE  /profilaziones/:id} : delete the "id" profilazione.
     *
     * @param id the id of the profilazione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/profilaziones/{id}")
    public ResponseEntity<Void> deleteProfilazione(@PathVariable Long id) {
        log.debug("REST request to delete Profilazione : {}", id);
        profilazioneRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
