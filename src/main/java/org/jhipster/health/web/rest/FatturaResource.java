package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Fattura;
import org.jhipster.health.repository.FatturaRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Fattura}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FatturaResource {

    private final Logger log = LoggerFactory.getLogger(FatturaResource.class);

    private static final String ENTITY_NAME = "fattura";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FatturaRepository fatturaRepository;

    public FatturaResource(FatturaRepository fatturaRepository) {
        this.fatturaRepository = fatturaRepository;
    }

    /**
     * {@code POST  /fatturas} : Create a new fattura.
     *
     * @param fattura the fattura to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fattura, or with status {@code 400 (Bad Request)} if the fattura has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fatturas")
    public ResponseEntity<Fattura> createFattura(@RequestBody Fattura fattura) throws URISyntaxException {
        log.debug("REST request to save Fattura : {}", fattura);
        if (fattura.getId() != null) {
            throw new BadRequestAlertException("A new fattura cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fattura result = fatturaRepository.save(fattura);
        return ResponseEntity.created(new URI("/api/fatturas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fatturas} : Updates an existing fattura.
     *
     * @param fattura the fattura to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fattura,
     * or with status {@code 400 (Bad Request)} if the fattura is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fattura couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fatturas")
    public ResponseEntity<Fattura> updateFattura(@RequestBody Fattura fattura) throws URISyntaxException {
        log.debug("REST request to update Fattura : {}", fattura);
        if (fattura.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Fattura result = fatturaRepository.save(fattura);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fattura.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fatturas} : get all the fatturas.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fatturas in body.
     */
    @GetMapping("/fatturas")
    public List<Fattura> getAllFatturas() {
        log.debug("REST request to get all Fatturas");
        return fatturaRepository.findAll();
    }

    /**
     * {@code GET  /fatturas/:id} : get the "id" fattura.
     *
     * @param id the id of the fattura to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fattura, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fatturas/{id}")
    public ResponseEntity<Fattura> getFattura(@PathVariable Long id) {
        log.debug("REST request to get Fattura : {}", id);
        Optional<Fattura> fattura = fatturaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fattura);
    }

    /**
     * {@code DELETE  /fatturas/:id} : delete the "id" fattura.
     *
     * @param id the id of the fattura to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fatturas/{id}")
    public ResponseEntity<Void> deleteFattura(@PathVariable Long id) {
        log.debug("REST request to delete Fattura : {}", id);
        fatturaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
