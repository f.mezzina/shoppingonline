package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Spedizione;
import org.jhipster.health.repository.SpedizioneRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Spedizione}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SpedizioneResource {

    private final Logger log = LoggerFactory.getLogger(SpedizioneResource.class);

    private static final String ENTITY_NAME = "spedizione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpedizioneRepository spedizioneRepository;

    public SpedizioneResource(SpedizioneRepository spedizioneRepository) {
        this.spedizioneRepository = spedizioneRepository;
    }

    /**
     * {@code POST  /spediziones} : Create a new spedizione.
     *
     * @param spedizione the spedizione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spedizione, or with status {@code 400 (Bad Request)} if the spedizione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spediziones")
    public ResponseEntity<Spedizione> createSpedizione(@RequestBody Spedizione spedizione) throws URISyntaxException {
        log.debug("REST request to save Spedizione : {}", spedizione);
        if (spedizione.getId() != null) {
            throw new BadRequestAlertException("A new spedizione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Spedizione result = spedizioneRepository.save(spedizione);
        return ResponseEntity.created(new URI("/api/spediziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spediziones} : Updates an existing spedizione.
     *
     * @param spedizione the spedizione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spedizione,
     * or with status {@code 400 (Bad Request)} if the spedizione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spedizione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spediziones")
    public ResponseEntity<Spedizione> updateSpedizione(@RequestBody Spedizione spedizione) throws URISyntaxException {
        log.debug("REST request to update Spedizione : {}", spedizione);
        if (spedizione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Spedizione result = spedizioneRepository.save(spedizione);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spedizione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /spediziones} : get all the spediziones.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spediziones in body.
     */
    @GetMapping("/spediziones")
    public List<Spedizione> getAllSpediziones() {
        log.debug("REST request to get all Spediziones");
        return spedizioneRepository.findAll();
    }

    /**
     * {@code GET  /spediziones/:id} : get the "id" spedizione.
     *
     * @param id the id of the spedizione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spedizione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spediziones/{id}")
    public ResponseEntity<Spedizione> getSpedizione(@PathVariable Long id) {
        log.debug("REST request to get Spedizione : {}", id);
        Optional<Spedizione> spedizione = spedizioneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(spedizione);
    }

    /**
     * {@code DELETE  /spediziones/:id} : delete the "id" spedizione.
     *
     * @param id the id of the spedizione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spediziones/{id}")
    public ResponseEntity<Void> deleteSpedizione(@PathVariable Long id) {
        log.debug("REST request to delete Spedizione : {}", id);
        spedizioneRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
