package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Elemento;
import org.jhipster.health.repository.ElementoRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Elemento}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ElementoResource {

    private final Logger log = LoggerFactory.getLogger(ElementoResource.class);

    private static final String ENTITY_NAME = "elemento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ElementoRepository elementoRepository;

    public ElementoResource(ElementoRepository elementoRepository) {
        this.elementoRepository = elementoRepository;
    }

    /**
     * {@code POST  /elementos} : Create a new elemento.
     *
     * @param elemento the elemento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new elemento, or with status {@code 400 (Bad Request)} if the elemento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/elementos")
    public ResponseEntity<Elemento> createElemento(@RequestBody Elemento elemento) throws URISyntaxException {
        log.debug("REST request to save Elemento : {}", elemento);
        if (elemento.getId() != null) {
            throw new BadRequestAlertException("A new elemento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Elemento result = elementoRepository.save(elemento);
        return ResponseEntity.created(new URI("/api/elementos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /elementos} : Updates an existing elemento.
     *
     * @param elemento the elemento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated elemento,
     * or with status {@code 400 (Bad Request)} if the elemento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the elemento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/elementos")
    public ResponseEntity<Elemento> updateElemento(@RequestBody Elemento elemento) throws URISyntaxException {
        log.debug("REST request to update Elemento : {}", elemento);
        if (elemento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Elemento result = elementoRepository.save(elemento);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, elemento.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /elementos} : get all the elementos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of elementos in body.
     */
    @GetMapping("/elementos")
    public List<Elemento> getAllElementos() {
        log.debug("REST request to get all Elementos");
        return elementoRepository.findAll();
    }

    /**
     * {@code GET  /elementos/:id} : get the "id" elemento.
     *
     * @param id the id of the elemento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the elemento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/elementos/{id}")
    public ResponseEntity<Elemento> getElemento(@PathVariable Long id) {
        log.debug("REST request to get Elemento : {}", id);
        Optional<Elemento> elemento = elementoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(elemento);
    }

    /**
     * {@code DELETE  /elementos/:id} : delete the "id" elemento.
     *
     * @param id the id of the elemento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/elementos/{id}")
    public ResponseEntity<Void> deleteElemento(@PathVariable Long id) {
        log.debug("REST request to delete Elemento : {}", id);
        elementoRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
