package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Configurazione;
import org.jhipster.health.repository.ConfigurazioneRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Configurazione}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ConfigurazioneResource {

    private final Logger log = LoggerFactory.getLogger(ConfigurazioneResource.class);

    private static final String ENTITY_NAME = "configurazione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConfigurazioneRepository configurazioneRepository;

    public ConfigurazioneResource(ConfigurazioneRepository configurazioneRepository) {
        this.configurazioneRepository = configurazioneRepository;
    }

    /**
     * {@code POST  /configuraziones} : Create a new configurazione.
     *
     * @param configurazione the configurazione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new configurazione, or with status {@code 400 (Bad Request)} if the configurazione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/configuraziones")
    public ResponseEntity<Configurazione> createConfigurazione(@RequestBody Configurazione configurazione) throws URISyntaxException {
        log.debug("REST request to save Configurazione : {}", configurazione);
        if (configurazione.getId() != null) {
            throw new BadRequestAlertException("A new configurazione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Configurazione result = configurazioneRepository.save(configurazione);
        return ResponseEntity.created(new URI("/api/configuraziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /configuraziones} : Updates an existing configurazione.
     *
     * @param configurazione the configurazione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configurazione,
     * or with status {@code 400 (Bad Request)} if the configurazione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the configurazione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/configuraziones")
    public ResponseEntity<Configurazione> updateConfigurazione(@RequestBody Configurazione configurazione) throws URISyntaxException {
        log.debug("REST request to update Configurazione : {}", configurazione);
        if (configurazione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Configurazione result = configurazioneRepository.save(configurazione);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, configurazione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /configuraziones} : get all the configuraziones.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of configuraziones in body.
     */
    @GetMapping("/configuraziones")
    public List<Configurazione> getAllConfiguraziones() {
        log.debug("REST request to get all Configuraziones");
        return configurazioneRepository.findAll();
    }

    /**
     * {@code GET  /configuraziones/:id} : get the "id" configurazione.
     *
     * @param id the id of the configurazione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the configurazione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/configuraziones/{id}")
    public ResponseEntity<Configurazione> getConfigurazione(@PathVariable Long id) {
        log.debug("REST request to get Configurazione : {}", id);
        Optional<Configurazione> configurazione = configurazioneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(configurazione);
    }

    /**
     * {@code DELETE  /configuraziones/:id} : delete the "id" configurazione.
     *
     * @param id the id of the configurazione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/configuraziones/{id}")
    public ResponseEntity<Void> deleteConfigurazione(@PathVariable Long id) {
        log.debug("REST request to delete Configurazione : {}", id);
        configurazioneRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
