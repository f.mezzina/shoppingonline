package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Fatturazione;
import org.jhipster.health.repository.FatturazioneRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Fatturazione}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FatturazioneResource {

    private final Logger log = LoggerFactory.getLogger(FatturazioneResource.class);

    private static final String ENTITY_NAME = "fatturazione";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FatturazioneRepository fatturazioneRepository;

    public FatturazioneResource(FatturazioneRepository fatturazioneRepository) {
        this.fatturazioneRepository = fatturazioneRepository;
    }

    /**
     * {@code POST  /fatturaziones} : Create a new fatturazione.
     *
     * @param fatturazione the fatturazione to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fatturazione, or with status {@code 400 (Bad Request)} if the fatturazione has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fatturaziones")
    public ResponseEntity<Fatturazione> createFatturazione(@RequestBody Fatturazione fatturazione) throws URISyntaxException {
        log.debug("REST request to save Fatturazione : {}", fatturazione);
        if (fatturazione.getId() != null) {
            throw new BadRequestAlertException("A new fatturazione cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Fatturazione result = fatturazioneRepository.save(fatturazione);
        return ResponseEntity.created(new URI("/api/fatturaziones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fatturaziones} : Updates an existing fatturazione.
     *
     * @param fatturazione the fatturazione to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fatturazione,
     * or with status {@code 400 (Bad Request)} if the fatturazione is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fatturazione couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fatturaziones")
    public ResponseEntity<Fatturazione> updateFatturazione(@RequestBody Fatturazione fatturazione) throws URISyntaxException {
        log.debug("REST request to update Fatturazione : {}", fatturazione);
        if (fatturazione.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Fatturazione result = fatturazioneRepository.save(fatturazione);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fatturazione.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fatturaziones} : get all the fatturaziones.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fatturaziones in body.
     */
    @GetMapping("/fatturaziones")
    public List<Fatturazione> getAllFatturaziones() {
        log.debug("REST request to get all Fatturaziones");
        return fatturazioneRepository.findAll();
    }

    /**
     * {@code GET  /fatturaziones/:id} : get the "id" fatturazione.
     *
     * @param id the id of the fatturazione to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fatturazione, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fatturaziones/{id}")
    public ResponseEntity<Fatturazione> getFatturazione(@PathVariable Long id) {
        log.debug("REST request to get Fatturazione : {}", id);
        Optional<Fatturazione> fatturazione = fatturazioneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fatturazione);
    }

    /**
     * {@code DELETE  /fatturaziones/:id} : delete the "id" fatturazione.
     *
     * @param id the id of the fatturazione to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fatturaziones/{id}")
    public ResponseEntity<Void> deleteFatturazione(@PathVariable Long id) {
        log.debug("REST request to delete Fatturazione : {}", id);
        fatturazioneRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
