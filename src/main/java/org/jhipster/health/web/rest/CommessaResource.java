package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Commessa;
import org.jhipster.health.repository.CommessaRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Commessa}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CommessaResource {

    private final Logger log = LoggerFactory.getLogger(CommessaResource.class);

    private static final String ENTITY_NAME = "commessa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommessaRepository commessaRepository;

    public CommessaResource(CommessaRepository commessaRepository) {
        this.commessaRepository = commessaRepository;
    }

    /**
     * {@code POST  /commessas} : Create a new commessa.
     *
     * @param commessa the commessa to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commessa, or with status {@code 400 (Bad Request)} if the commessa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commessas")
    public ResponseEntity<Commessa> createCommessa(@RequestBody Commessa commessa) throws URISyntaxException {
        log.debug("REST request to save Commessa : {}", commessa);
        if (commessa.getId() != null) {
            throw new BadRequestAlertException("A new commessa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Commessa result = commessaRepository.save(commessa);
        return ResponseEntity.created(new URI("/api/commessas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commessas} : Updates an existing commessa.
     *
     * @param commessa the commessa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commessa,
     * or with status {@code 400 (Bad Request)} if the commessa is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commessa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commessas")
    public ResponseEntity<Commessa> updateCommessa(@RequestBody Commessa commessa) throws URISyntaxException {
        log.debug("REST request to update Commessa : {}", commessa);
        if (commessa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Commessa result = commessaRepository.save(commessa);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, commessa.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /commessas} : get all the commessas.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commessas in body.
     */
    @GetMapping("/commessas")
    public List<Commessa> getAllCommessas() {
        log.debug("REST request to get all Commessas");
        return commessaRepository.findAll();
    }

    /**
     * {@code GET  /commessas/:id} : get the "id" commessa.
     *
     * @param id the id of the commessa to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commessa, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commessas/{id}")
    public ResponseEntity<Commessa> getCommessa(@PathVariable Long id) {
        log.debug("REST request to get Commessa : {}", id);
        Optional<Commessa> commessa = commessaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(commessa);
    }

    /**
     * {@code DELETE  /commessas/:id} : delete the "id" commessa.
     *
     * @param id the id of the commessa to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commessas/{id}")
    public ResponseEntity<Void> deleteCommessa(@PathVariable Long id) {
        log.debug("REST request to delete Commessa : {}", id);
        commessaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
