package org.jhipster.health.web.rest;

import org.jhipster.health.domain.Computer;
import org.jhipster.health.repository.ComputerRepository;
import org.jhipster.health.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.jhipster.health.domain.Computer}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ComputerResource {

    private final Logger log = LoggerFactory.getLogger(ComputerResource.class);

    private static final String ENTITY_NAME = "computer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ComputerRepository computerRepository;

    public ComputerResource(ComputerRepository computerRepository) {
        this.computerRepository = computerRepository;
    }

    /**
     * {@code POST  /computers} : Create a new computer.
     *
     * @param computer the computer to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new computer, or with status {@code 400 (Bad Request)} if the computer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/computers")
    public ResponseEntity<Computer> createComputer(@RequestBody Computer computer) throws URISyntaxException {
        log.debug("REST request to save Computer : {}", computer);
        if (computer.getId() != null) {
            throw new BadRequestAlertException("A new computer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Computer result = computerRepository.save(computer);
        return ResponseEntity.created(new URI("/api/computers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /computers} : Updates an existing computer.
     *
     * @param computer the computer to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated computer,
     * or with status {@code 400 (Bad Request)} if the computer is not valid,
     * or with status {@code 500 (Internal Server Error)} if the computer couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/computers")
    public ResponseEntity<Computer> updateComputer(@RequestBody Computer computer) throws URISyntaxException {
        log.debug("REST request to update Computer : {}", computer);
        if (computer.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Computer result = computerRepository.save(computer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, computer.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /computers} : get all the computers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of computers in body.
     */
    @GetMapping("/computers")
    public List<Computer> getAllComputers() {
        log.debug("REST request to get all Computers");
        return computerRepository.findAll();
    }

    /**
     * {@code GET  /computers/:id} : get the "id" computer.
     *
     * @param id the id of the computer to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the computer, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/computers/{id}")
    public ResponseEntity<Computer> getComputer(@PathVariable Long id) {
        log.debug("REST request to get Computer : {}", id);
        Optional<Computer> computer = computerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(computer);
    }

    /**
     * {@code DELETE  /computers/:id} : delete the "id" computer.
     *
     * @param id the id of the computer to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/computers/{id}")
    public ResponseEntity<Void> deleteComputer(@PathVariable Long id) {
        log.debug("REST request to delete Computer : {}", id);
        computerRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
