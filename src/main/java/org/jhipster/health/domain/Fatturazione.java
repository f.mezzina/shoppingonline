package org.jhipster.health.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A Fatturazione.
 */
@Entity
@Table(name = "fatturazione")
public class Fatturazione implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(unique = true)
    private Fattura fattura;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fattura getFattura() {
        return fattura;
    }

    public Fatturazione fattura(Fattura fattura) {
        this.fattura = fattura;
        return this;
    }

    public void setFattura(Fattura fattura) {
        this.fattura = fattura;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fatturazione)) {
            return false;
        }
        return id != null && id.equals(((Fatturazione) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fatturazione{" +
            "id=" + getId() +
            "}";
    }
}
