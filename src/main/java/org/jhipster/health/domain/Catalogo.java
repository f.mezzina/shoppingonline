package org.jhipster.health.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Catalogo.
 */
@Entity
@Table(name = "catalogo")
public class Catalogo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "catalogo")
    private Set<Configurazione> configuraziones = new HashSet<>();

    @OneToMany(mappedBy = "catalogo")
    private Set<Elemento> elementos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Configurazione> getConfiguraziones() {
        return configuraziones;
    }

    public Catalogo configuraziones(Set<Configurazione> configuraziones) {
        this.configuraziones = configuraziones;
        return this;
    }

    public Catalogo addConfigurazione(Configurazione configurazione) {
        this.configuraziones.add(configurazione);
        configurazione.setCatalogo(this);
        return this;
    }

    public Catalogo removeConfigurazione(Configurazione configurazione) {
        this.configuraziones.remove(configurazione);
        configurazione.setCatalogo(null);
        return this;
    }

    public void setConfiguraziones(Set<Configurazione> configuraziones) {
        this.configuraziones = configuraziones;
    }

    public Set<Elemento> getElementos() {
        return elementos;
    }

    public Catalogo elementos(Set<Elemento> elementos) {
        this.elementos = elementos;
        return this;
    }

    public Catalogo addElemento(Elemento elemento) {
        this.elementos.add(elemento);
        elemento.setCatalogo(this);
        return this;
    }

    public Catalogo removeElemento(Elemento elemento) {
        this.elementos.remove(elemento);
        elemento.setCatalogo(null);
        return this;
    }

    public void setElementos(Set<Elemento> elementos) {
        this.elementos = elementos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Catalogo)) {
            return false;
        }
        return id != null && id.equals(((Catalogo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Catalogo{" +
            "id=" + getId() +
            "}";
    }
}
