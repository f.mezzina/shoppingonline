package org.jhipster.health.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A OperatoreAdv.
 */
@Entity
@Table(name = "operatore_adv")
public class OperatoreAdv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "agenzia")
    private String agenzia;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "autenticato")
    private Boolean autenticato;

    @OneToOne
    @JoinColumn(unique = true)
    private Utente utente;

    @ManyToOne
    @JsonIgnoreProperties(value = "operatoreAdvs", allowSetters = true)
    private Azienda azienda;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public OperatoreAdv nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public OperatoreAdv cognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getAgenzia() {
        return agenzia;
    }

    public OperatoreAdv agenzia(String agenzia) {
        this.agenzia = agenzia;
        return this;
    }

    public void setAgenzia(String agenzia) {
        this.agenzia = agenzia;
    }

    public String getUsername() {
        return username;
    }

    public OperatoreAdv username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public OperatoreAdv password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isAutenticato() {
        return autenticato;
    }

    public OperatoreAdv autenticato(Boolean autenticato) {
        this.autenticato = autenticato;
        return this;
    }

    public void setAutenticato(Boolean autenticato) {
        this.autenticato = autenticato;
    }

    public Utente getUtente() {
        return utente;
    }

    public OperatoreAdv utente(Utente utente) {
        this.utente = utente;
        return this;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Azienda getAzienda() {
        return azienda;
    }

    public OperatoreAdv azienda(Azienda azienda) {
        this.azienda = azienda;
        return this;
    }

    public void setAzienda(Azienda azienda) {
        this.azienda = azienda;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OperatoreAdv)) {
            return false;
        }
        return id != null && id.equals(((OperatoreAdv) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OperatoreAdv{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", cognome='" + getCognome() + "'" +
            ", agenzia='" + getAgenzia() + "'" +
            ", username='" + getUsername() + "'" +
            ", password='" + getPassword() + "'" +
            ", autenticato='" + isAutenticato() + "'" +
            "}";
    }
}
