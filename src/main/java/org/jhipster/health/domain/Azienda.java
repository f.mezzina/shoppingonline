package org.jhipster.health.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A Azienda.
 */
@Entity
@Table(name = "azienda")
public class Azienda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "agenzia")
    private String agenzia;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Azienda nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public Azienda cognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getAgenzia() {
        return agenzia;
    }

    public Azienda agenzia(String agenzia) {
        this.agenzia = agenzia;
        return this;
    }

    public void setAgenzia(String agenzia) {
        this.agenzia = agenzia;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Azienda)) {
            return false;
        }
        return id != null && id.equals(((Azienda) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Azienda{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", cognome='" + getCognome() + "'" +
            ", agenzia='" + getAgenzia() + "'" +
            "}";
    }
}
