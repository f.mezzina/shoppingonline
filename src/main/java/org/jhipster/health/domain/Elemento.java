package org.jhipster.health.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Elemento.
 */
@Entity
@Table(name = "elemento")
public class Elemento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tipologia")
    private String tipologia;

    @Column(name = "prezzo", precision = 21, scale = 2)
    private BigDecimal prezzo;

    @Column(name = "descrizione")
    private String descrizione;

    @ManyToOne
    @JsonIgnoreProperties(value = "elementos", allowSetters = true)
    private Configurazione configurazione;

    @ManyToOne
    @JsonIgnoreProperties(value = "elementos", allowSetters = true)
    private Catalogo catalogo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipologia() {
        return tipologia;
    }

    public Elemento tipologia(String tipologia) {
        this.tipologia = tipologia;
        return this;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public BigDecimal getPrezzo() {
        return prezzo;
    }

    public Elemento prezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public Elemento descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Configurazione getConfigurazione() {
        return configurazione;
    }

    public Elemento configurazione(Configurazione configurazione) {
        this.configurazione = configurazione;
        return this;
    }

    public void setConfigurazione(Configurazione configurazione) {
        this.configurazione = configurazione;
    }

    public Catalogo getCatalogo() {
        return catalogo;
    }

    public Elemento catalogo(Catalogo catalogo) {
        this.catalogo = catalogo;
        return this;
    }

    public void setCatalogo(Catalogo catalogo) {
        this.catalogo = catalogo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Elemento)) {
            return false;
        }
        return id != null && id.equals(((Elemento) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Elemento{" +
            "id=" + getId() +
            ", tipologia='" + getTipologia() + "'" +
            ", prezzo=" + getPrezzo() +
            ", descrizione='" + getDescrizione() + "'" +
            "}";
    }
}
