package org.jhipster.health.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Configurazione.
 */
@Entity
@Table(name = "configurazione")
public class Configurazione implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tipologia")
    private String tipologia;

    @OneToMany(mappedBy = "configurazione")
    private Set<Elemento> elementos = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "configuraziones", allowSetters = true)
    private Catalogo catalogo;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipologia() {
        return tipologia;
    }

    public Configurazione tipologia(String tipologia) {
        this.tipologia = tipologia;
        return this;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public Set<Elemento> getElementos() {
        return elementos;
    }

    public Configurazione elementos(Set<Elemento> elementos) {
        this.elementos = elementos;
        return this;
    }

    public Configurazione addElemento(Elemento elemento) {
        this.elementos.add(elemento);
        elemento.setConfigurazione(this);
        return this;
    }

    public Configurazione removeElemento(Elemento elemento) {
        this.elementos.remove(elemento);
        elemento.setConfigurazione(null);
        return this;
    }

    public void setElementos(Set<Elemento> elementos) {
        this.elementos = elementos;
    }

    public Catalogo getCatalogo() {
        return catalogo;
    }

    public Configurazione catalogo(Catalogo catalogo) {
        this.catalogo = catalogo;
        return this;
    }

    public void setCatalogo(Catalogo catalogo) {
        this.catalogo = catalogo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Configurazione)) {
            return false;
        }
        return id != null && id.equals(((Configurazione) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Configurazione{" +
            "id=" + getId() +
            ", tipologia='" + getTipologia() + "'" +
            "}";
    }
}
