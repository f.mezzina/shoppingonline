package org.jhipster.health.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Computer.
 */
@Entity
@Table(name = "computer")
public class Computer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "prezzo", precision = 21, scale = 2)
    private BigDecimal prezzo;

    @OneToOne
    @JoinColumn(unique = true)
    private Configurazione configurazione;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrezzo() {
        return prezzo;
    }

    public Computer prezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    public Configurazione getConfigurazione() {
        return configurazione;
    }

    public Computer configurazione(Configurazione configurazione) {
        this.configurazione = configurazione;
        return this;
    }

    public void setConfigurazione(Configurazione configurazione) {
        this.configurazione = configurazione;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Computer)) {
            return false;
        }
        return id != null && id.equals(((Computer) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Computer{" +
            "id=" + getId() +
            ", prezzo=" + getPrezzo() +
            "}";
    }
}
