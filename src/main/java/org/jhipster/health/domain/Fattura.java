package org.jhipster.health.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A Fattura.
 */
@Entity
@Table(name = "fattura")
public class Fattura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "partita_iva")
    private String partitaIva;

    @Column(name = "ragione_sociale")
    private String ragioneSociale;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public Fattura partitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
        return this;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public Fattura ragioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
        return this;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fattura)) {
            return false;
        }
        return id != null && id.equals(((Fattura) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fattura{" +
            "id=" + getId() +
            ", partitaIva='" + getPartitaIva() + "'" +
            ", ragioneSociale='" + getRagioneSociale() + "'" +
            "}";
    }
}
