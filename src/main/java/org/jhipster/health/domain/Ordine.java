package org.jhipster.health.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Ordine.
 */
@Entity
@Table(name = "ordine")
public class Ordine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "stato")
    private String stato;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "data")
    private LocalDate data;

    @OneToOne
    @JoinColumn(unique = true)
    private Pagamento pagamento;

    @OneToOne
    @JoinColumn(unique = true)
    private Spedizione spedizione;

    @OneToOne
    @JoinColumn(unique = true)
    private Fattura fattura;

    @OneToOne
    @JoinColumn(unique = true)
    private Computer computer;

    @ManyToOne
    @JsonIgnoreProperties(value = "ordines", allowSetters = true)
    private Commessa commessa;

    @ManyToOne
    @JsonIgnoreProperties(value = "ordines", allowSetters = true)
    private Cliente cliente;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStato() {
        return stato;
    }

    public Ordine stato(String stato) {
        this.stato = stato;
        return this;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public Integer getNumero() {
        return numero;
    }

    public Ordine numero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public LocalDate getData() {
        return data;
    }

    public Ordine data(LocalDate data) {
        this.data = data;
        return this;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public Ordine pagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
        return this;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public Spedizione getSpedizione() {
        return spedizione;
    }

    public Ordine spedizione(Spedizione spedizione) {
        this.spedizione = spedizione;
        return this;
    }

    public void setSpedizione(Spedizione spedizione) {
        this.spedizione = spedizione;
    }

    public Fattura getFattura() {
        return fattura;
    }

    public Ordine fattura(Fattura fattura) {
        this.fattura = fattura;
        return this;
    }

    public void setFattura(Fattura fattura) {
        this.fattura = fattura;
    }

    public Computer getComputer() {
        return computer;
    }

    public Ordine computer(Computer computer) {
        this.computer = computer;
        return this;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public Commessa getCommessa() {
        return commessa;
    }

    public Ordine commessa(Commessa commessa) {
        this.commessa = commessa;
        return this;
    }

    public void setCommessa(Commessa commessa) {
        this.commessa = commessa;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Ordine cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ordine)) {
            return false;
        }
        return id != null && id.equals(((Ordine) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ordine{" +
            "id=" + getId() +
            ", stato='" + getStato() + "'" +
            ", numero=" + getNumero() +
            ", data='" + getData() + "'" +
            "}";
    }
}
