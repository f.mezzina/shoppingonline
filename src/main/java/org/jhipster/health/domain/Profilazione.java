package org.jhipster.health.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A Profilazione.
 */
@Entity
@Table(name = "profilazione")
public class Profilazione implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(unique = true)
    private OperatoreAdv operatoreAdv;

    @OneToOne
    @JoinColumn(unique = true)
    private Cliente cliente;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OperatoreAdv getOperatoreAdv() {
        return operatoreAdv;
    }

    public Profilazione operatoreAdv(OperatoreAdv operatoreAdv) {
        this.operatoreAdv = operatoreAdv;
        return this;
    }

    public void setOperatoreAdv(OperatoreAdv operatoreAdv) {
        this.operatoreAdv = operatoreAdv;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Profilazione cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profilazione)) {
            return false;
        }
        return id != null && id.equals(((Profilazione) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Profilazione{" +
            "id=" + getId() +
            "}";
    }
}
