package org.jhipster.health.repository;

import org.jhipster.health.domain.Elemento;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Elemento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElementoRepository extends JpaRepository<Elemento, Long> {
}
