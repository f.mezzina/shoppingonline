package org.jhipster.health.repository;

import org.jhipster.health.domain.Profilazione;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Profilazione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfilazioneRepository extends JpaRepository<Profilazione, Long> {
}
