package org.jhipster.health.repository;

import org.jhipster.health.domain.Computer;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Computer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComputerRepository extends JpaRepository<Computer, Long> {
}
