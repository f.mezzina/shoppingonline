package org.jhipster.health.repository;

import org.jhipster.health.domain.Fatturazione;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Fatturazione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FatturazioneRepository extends JpaRepository<Fatturazione, Long> {
}
