package org.jhipster.health.repository;

import org.jhipster.health.domain.Spedizione;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Spedizione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpedizioneRepository extends JpaRepository<Spedizione, Long> {
}
