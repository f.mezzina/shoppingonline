package org.jhipster.health.repository;

import org.jhipster.health.domain.Configurazione;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Configurazione entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigurazioneRepository extends JpaRepository<Configurazione, Long> {
}
