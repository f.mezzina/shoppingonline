package org.jhipster.health.repository;

import org.jhipster.health.domain.OperatoreAdv;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OperatoreAdv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OperatoreAdvRepository extends JpaRepository<OperatoreAdv, Long> {
}
