package org.jhipster.health.repository;

import org.jhipster.health.domain.Fattura;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Fattura entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FatturaRepository extends JpaRepository<Fattura, Long> {
}
