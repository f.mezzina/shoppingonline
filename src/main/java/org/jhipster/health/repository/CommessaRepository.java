package org.jhipster.health.repository;

import org.jhipster.health.domain.Commessa;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Commessa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommessaRepository extends JpaRepository<Commessa, Long> {
}
