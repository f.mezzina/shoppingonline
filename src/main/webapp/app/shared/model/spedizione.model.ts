export interface ISpedizione {
  id?: number;
  indirizzo?: string;
}

export class Spedizione implements ISpedizione {
  constructor(public id?: number, public indirizzo?: string) {}
}
