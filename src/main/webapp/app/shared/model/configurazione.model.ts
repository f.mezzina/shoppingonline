import { IElemento } from 'app/shared/model/elemento.model';
import { ICatalogo } from 'app/shared/model/catalogo.model';

export interface IConfigurazione {
  id?: number;
  tipologia?: string;
  elementos?: IElemento[];
  catalogo?: ICatalogo;
}

export class Configurazione implements IConfigurazione {
  constructor(public id?: number, public tipologia?: string, public elementos?: IElemento[], public catalogo?: ICatalogo) {}
}
