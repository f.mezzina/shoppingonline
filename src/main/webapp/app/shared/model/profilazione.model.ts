import { IOperatoreAdv } from 'app/shared/model/operatore-adv.model';
import { ICliente } from 'app/shared/model/cliente.model';

export interface IProfilazione {
  id?: number;
  operatoreAdv?: IOperatoreAdv;
  cliente?: ICliente;
}

export class Profilazione implements IProfilazione {
  constructor(public id?: number, public operatoreAdv?: IOperatoreAdv, public cliente?: ICliente) {}
}
