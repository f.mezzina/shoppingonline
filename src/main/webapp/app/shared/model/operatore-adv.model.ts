import { IUtente } from 'app/shared/model/utente.model';
import { IAzienda } from 'app/shared/model/azienda.model';

export interface IOperatoreAdv {
  id?: number;
  nome?: string;
  cognome?: string;
  agenzia?: string;
  username?: string;
  password?: string;
  autenticato?: boolean;
  utente?: IUtente;
  azienda?: IAzienda;
}

export class OperatoreAdv implements IOperatoreAdv {
  constructor(
    public id?: number,
    public nome?: string,
    public cognome?: string,
    public agenzia?: string,
    public username?: string,
    public password?: string,
    public autenticato?: boolean,
    public utente?: IUtente,
    public azienda?: IAzienda
  ) {
    this.autenticato = this.autenticato || false;
  }
}
