import { Moment } from 'moment';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { ISpedizione } from 'app/shared/model/spedizione.model';
import { IFattura } from 'app/shared/model/fattura.model';
import { IComputer } from 'app/shared/model/computer.model';
import { ICommessa } from 'app/shared/model/commessa.model';
import { ICliente } from 'app/shared/model/cliente.model';

export interface IOrdine {
  id?: number;
  stato?: string;
  numero?: number;
  data?: Moment;
  pagamento?: IPagamento;
  spedizione?: ISpedizione;
  fattura?: IFattura;
  computer?: IComputer;
  commessa?: ICommessa;
  cliente?: ICliente;
}

export class Ordine implements IOrdine {
  constructor(
    public id?: number,
    public stato?: string,
    public numero?: number,
    public data?: Moment,
    public pagamento?: IPagamento,
    public spedizione?: ISpedizione,
    public fattura?: IFattura,
    public computer?: IComputer,
    public commessa?: ICommessa,
    public cliente?: ICliente
  ) {}
}
