import { IFattura } from 'app/shared/model/fattura.model';

export interface IFatturazione {
  id?: number;
  fattura?: IFattura;
}

export class Fatturazione implements IFatturazione {
  constructor(public id?: number, public fattura?: IFattura) {}
}
