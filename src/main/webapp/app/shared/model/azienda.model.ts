export interface IAzienda {
  id?: number;
  nome?: string;
  cognome?: string;
  agenzia?: string;
}

export class Azienda implements IAzienda {
  constructor(public id?: number, public nome?: string, public cognome?: string, public agenzia?: string) {}
}
