import { IConfigurazione } from 'app/shared/model/configurazione.model';
import { IElemento } from 'app/shared/model/elemento.model';

export interface ICatalogo {
  id?: number;
  configuraziones?: IConfigurazione[];
  elementos?: IElemento[];
}

export class Catalogo implements ICatalogo {
  constructor(public id?: number, public configuraziones?: IConfigurazione[], public elementos?: IElemento[]) {}
}
