export interface IPagamento {
  id?: number;
  tipo?: string;
}

export class Pagamento implements IPagamento {
  constructor(public id?: number, public tipo?: string) {}
}
