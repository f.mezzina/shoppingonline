export interface IFattura {
  id?: number;
  partitaIva?: string;
  ragioneSociale?: string;
}

export class Fattura implements IFattura {
  constructor(public id?: number, public partitaIva?: string, public ragioneSociale?: string) {}
}
