import { IConfigurazione } from 'app/shared/model/configurazione.model';
import { ICatalogo } from 'app/shared/model/catalogo.model';

export interface IElemento {
  id?: number;
  tipologia?: string;
  prezzo?: number;
  descrizione?: string;
  configurazione?: IConfigurazione;
  catalogo?: ICatalogo;
}

export class Elemento implements IElemento {
  constructor(
    public id?: number,
    public tipologia?: string,
    public prezzo?: number,
    public descrizione?: string,
    public configurazione?: IConfigurazione,
    public catalogo?: ICatalogo
  ) {}
}
