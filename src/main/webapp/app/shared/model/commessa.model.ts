export interface ICommessa {
  id?: number;
}

export class Commessa implements ICommessa {
  constructor(public id?: number) {}
}
