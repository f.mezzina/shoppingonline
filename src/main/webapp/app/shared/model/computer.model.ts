import { IConfigurazione } from 'app/shared/model/configurazione.model';

export interface IComputer {
  id?: number;
  prezzo?: number;
  configurazione?: IConfigurazione;
}

export class Computer implements IComputer {
  constructor(public id?: number, public prezzo?: number, public configurazione?: IConfigurazione) {}
}
