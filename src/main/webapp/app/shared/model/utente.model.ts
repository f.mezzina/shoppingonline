export interface IUtente {
  id?: number;
}

export class Utente implements IUtente {
  constructor(public id?: number) {}
}
