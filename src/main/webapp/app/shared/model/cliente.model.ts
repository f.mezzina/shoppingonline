import { IUtente } from 'app/shared/model/utente.model';

export interface ICliente {
  id?: number;
  nome?: string;
  cognome?: string;
  username?: string;
  password?: string;
  autenticato?: boolean;
  utente?: IUtente;
}

export class Cliente implements ICliente {
  constructor(
    public id?: number,
    public nome?: string,
    public cognome?: string,
    public username?: string,
    public password?: string,
    public autenticato?: boolean,
    public utente?: IUtente
  ) {
    this.autenticato = this.autenticato || false;
  }
}
