import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { SpedizioneComponent } from './spedizione.component';
import { SpedizioneDetailComponent } from './spedizione-detail.component';
import { SpedizioneUpdateComponent } from './spedizione-update.component';
import { SpedizioneDeleteDialogComponent } from './spedizione-delete-dialog.component';
import { spedizioneRoute } from './spedizione.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(spedizioneRoute)],
  declarations: [SpedizioneComponent, SpedizioneDetailComponent, SpedizioneUpdateComponent, SpedizioneDeleteDialogComponent],
  entryComponents: [SpedizioneDeleteDialogComponent],
})
export class ShoppingonlineSpedizioneModule {}
