import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISpedizione } from 'app/shared/model/spedizione.model';
import { SpedizioneService } from './spedizione.service';
import { SpedizioneDeleteDialogComponent } from './spedizione-delete-dialog.component';

@Component({
  selector: 'jhi-spedizione',
  templateUrl: './spedizione.component.html',
})
export class SpedizioneComponent implements OnInit, OnDestroy {
  spediziones?: ISpedizione[];
  eventSubscriber?: Subscription;

  constructor(protected spedizioneService: SpedizioneService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.spedizioneService.query().subscribe((res: HttpResponse<ISpedizione[]>) => (this.spediziones = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSpediziones();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISpedizione): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSpediziones(): void {
    this.eventSubscriber = this.eventManager.subscribe('spedizioneListModification', () => this.loadAll());
  }

  delete(spedizione: ISpedizione): void {
    const modalRef = this.modalService.open(SpedizioneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.spedizione = spedizione;
  }
}
