import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISpedizione, Spedizione } from 'app/shared/model/spedizione.model';
import { SpedizioneService } from './spedizione.service';
import { SpedizioneComponent } from './spedizione.component';
import { SpedizioneDetailComponent } from './spedizione-detail.component';
import { SpedizioneUpdateComponent } from './spedizione-update.component';

@Injectable({ providedIn: 'root' })
export class SpedizioneResolve implements Resolve<ISpedizione> {
  constructor(private service: SpedizioneService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpedizione> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((spedizione: HttpResponse<Spedizione>) => {
          if (spedizione.body) {
            return of(spedizione.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Spedizione());
  }
}

export const spedizioneRoute: Routes = [
  {
    path: '',
    component: SpedizioneComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.spedizione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SpedizioneDetailComponent,
    resolve: {
      spedizione: SpedizioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.spedizione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SpedizioneUpdateComponent,
    resolve: {
      spedizione: SpedizioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.spedizione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SpedizioneUpdateComponent,
    resolve: {
      spedizione: SpedizioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.spedizione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
