import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpedizione } from 'app/shared/model/spedizione.model';

@Component({
  selector: 'jhi-spedizione-detail',
  templateUrl: './spedizione-detail.component.html',
})
export class SpedizioneDetailComponent implements OnInit {
  spedizione: ISpedizione | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spedizione }) => (this.spedizione = spedizione));
  }

  previousState(): void {
    window.history.back();
  }
}
