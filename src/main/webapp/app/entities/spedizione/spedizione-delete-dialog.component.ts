import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpedizione } from 'app/shared/model/spedizione.model';
import { SpedizioneService } from './spedizione.service';

@Component({
  templateUrl: './spedizione-delete-dialog.component.html',
})
export class SpedizioneDeleteDialogComponent {
  spedizione?: ISpedizione;

  constructor(
    protected spedizioneService: SpedizioneService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.spedizioneService.delete(id).subscribe(() => {
      this.eventManager.broadcast('spedizioneListModification');
      this.activeModal.close();
    });
  }
}
