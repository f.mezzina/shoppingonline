import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISpedizione } from 'app/shared/model/spedizione.model';

type EntityResponseType = HttpResponse<ISpedizione>;
type EntityArrayResponseType = HttpResponse<ISpedizione[]>;

@Injectable({ providedIn: 'root' })
export class SpedizioneService {
  public resourceUrl = SERVER_API_URL + 'api/spediziones';

  constructor(protected http: HttpClient) {}

  create(spedizione: ISpedizione): Observable<EntityResponseType> {
    return this.http.post<ISpedizione>(this.resourceUrl, spedizione, { observe: 'response' });
  }

  update(spedizione: ISpedizione): Observable<EntityResponseType> {
    return this.http.put<ISpedizione>(this.resourceUrl, spedizione, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISpedizione>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISpedizione[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
