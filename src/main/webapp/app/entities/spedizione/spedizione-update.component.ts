import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISpedizione, Spedizione } from 'app/shared/model/spedizione.model';
import { SpedizioneService } from './spedizione.service';

@Component({
  selector: 'jhi-spedizione-update',
  templateUrl: './spedizione-update.component.html',
})
export class SpedizioneUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    indirizzo: [],
  });

  constructor(protected spedizioneService: SpedizioneService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ spedizione }) => {
      this.updateForm(spedizione);
    });
  }

  updateForm(spedizione: ISpedizione): void {
    this.editForm.patchValue({
      id: spedizione.id,
      indirizzo: spedizione.indirizzo,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const spedizione = this.createFromForm();
    if (spedizione.id !== undefined) {
      this.subscribeToSaveResponse(this.spedizioneService.update(spedizione));
    } else {
      this.subscribeToSaveResponse(this.spedizioneService.create(spedizione));
    }
  }

  private createFromForm(): ISpedizione {
    return {
      ...new Spedizione(),
      id: this.editForm.get(['id'])!.value,
      indirizzo: this.editForm.get(['indirizzo'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpedizione>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
