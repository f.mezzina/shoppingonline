import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IComputer, Computer } from 'app/shared/model/computer.model';
import { ComputerService } from './computer.service';
import { ComputerComponent } from './computer.component';
import { ComputerDetailComponent } from './computer-detail.component';
import { ComputerUpdateComponent } from './computer-update.component';

@Injectable({ providedIn: 'root' })
export class ComputerResolve implements Resolve<IComputer> {
  constructor(private service: ComputerService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IComputer> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((computer: HttpResponse<Computer>) => {
          if (computer.body) {
            return of(computer.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Computer());
  }
}

export const computerRoute: Routes = [
  {
    path: '',
    component: ComputerComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.computer.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ComputerDetailComponent,
    resolve: {
      computer: ComputerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.computer.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ComputerUpdateComponent,
    resolve: {
      computer: ComputerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.computer.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ComputerUpdateComponent,
    resolve: {
      computer: ComputerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.computer.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
