import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IComputer } from 'app/shared/model/computer.model';
import { ComputerService } from './computer.service';
import { ComputerDeleteDialogComponent } from './computer-delete-dialog.component';

@Component({
  selector: 'jhi-computer',
  templateUrl: './computer.component.html',
})
export class ComputerComponent implements OnInit, OnDestroy {
  computers?: IComputer[];
  eventSubscriber?: Subscription;

  constructor(protected computerService: ComputerService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.computerService.query().subscribe((res: HttpResponse<IComputer[]>) => (this.computers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInComputers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IComputer): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInComputers(): void {
    this.eventSubscriber = this.eventManager.subscribe('computerListModification', () => this.loadAll());
  }

  delete(computer: IComputer): void {
    const modalRef = this.modalService.open(ComputerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.computer = computer;
  }
}
