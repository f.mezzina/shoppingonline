import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { ComputerComponent } from './computer.component';
import { ComputerDetailComponent } from './computer-detail.component';
import { ComputerUpdateComponent } from './computer-update.component';
import { ComputerDeleteDialogComponent } from './computer-delete-dialog.component';
import { computerRoute } from './computer.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(computerRoute)],
  declarations: [ComputerComponent, ComputerDetailComponent, ComputerUpdateComponent, ComputerDeleteDialogComponent],
  entryComponents: [ComputerDeleteDialogComponent],
})
export class ShoppingonlineComputerModule {}
