import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IComputer, Computer } from 'app/shared/model/computer.model';
import { ComputerService } from './computer.service';
import { IConfigurazione } from 'app/shared/model/configurazione.model';
import { ConfigurazioneService } from 'app/entities/configurazione/configurazione.service';

@Component({
  selector: 'jhi-computer-update',
  templateUrl: './computer-update.component.html',
})
export class ComputerUpdateComponent implements OnInit {
  isSaving = false;
  configuraziones: IConfigurazione[] = [];

  editForm = this.fb.group({
    id: [],
    prezzo: [],
    configurazione: [],
  });

  constructor(
    protected computerService: ComputerService,
    protected configurazioneService: ConfigurazioneService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ computer }) => {
      this.updateForm(computer);

      this.configurazioneService
        .query({ filter: 'computer-is-null' })
        .pipe(
          map((res: HttpResponse<IConfigurazione[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IConfigurazione[]) => {
          if (!computer.configurazione || !computer.configurazione.id) {
            this.configuraziones = resBody;
          } else {
            this.configurazioneService
              .find(computer.configurazione.id)
              .pipe(
                map((subRes: HttpResponse<IConfigurazione>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IConfigurazione[]) => (this.configuraziones = concatRes));
          }
        });
    });
  }

  updateForm(computer: IComputer): void {
    this.editForm.patchValue({
      id: computer.id,
      prezzo: computer.prezzo,
      configurazione: computer.configurazione,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const computer = this.createFromForm();
    if (computer.id !== undefined) {
      this.subscribeToSaveResponse(this.computerService.update(computer));
    } else {
      this.subscribeToSaveResponse(this.computerService.create(computer));
    }
  }

  private createFromForm(): IComputer {
    return {
      ...new Computer(),
      id: this.editForm.get(['id'])!.value,
      prezzo: this.editForm.get(['prezzo'])!.value,
      configurazione: this.editForm.get(['configurazione'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComputer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IConfigurazione): any {
    return item.id;
  }
}
