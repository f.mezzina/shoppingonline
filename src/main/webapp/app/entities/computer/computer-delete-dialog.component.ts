import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IComputer } from 'app/shared/model/computer.model';
import { ComputerService } from './computer.service';

@Component({
  templateUrl: './computer-delete-dialog.component.html',
})
export class ComputerDeleteDialogComponent {
  computer?: IComputer;

  constructor(protected computerService: ComputerService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.computerService.delete(id).subscribe(() => {
      this.eventManager.broadcast('computerListModification');
      this.activeModal.close();
    });
  }
}
