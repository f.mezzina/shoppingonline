import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IComputer } from 'app/shared/model/computer.model';

type EntityResponseType = HttpResponse<IComputer>;
type EntityArrayResponseType = HttpResponse<IComputer[]>;

@Injectable({ providedIn: 'root' })
export class ComputerService {
  public resourceUrl = SERVER_API_URL + 'api/computers';

  constructor(protected http: HttpClient) {}

  create(computer: IComputer): Observable<EntityResponseType> {
    return this.http.post<IComputer>(this.resourceUrl, computer, { observe: 'response' });
  }

  update(computer: IComputer): Observable<EntityResponseType> {
    return this.http.put<IComputer>(this.resourceUrl, computer, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IComputer>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IComputer[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
