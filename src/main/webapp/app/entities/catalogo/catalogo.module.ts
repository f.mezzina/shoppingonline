import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { CatalogoComponent } from './catalogo.component';
import { CatalogoDetailComponent } from './catalogo-detail.component';
import { CatalogoUpdateComponent } from './catalogo-update.component';
import { CatalogoDeleteDialogComponent } from './catalogo-delete-dialog.component';
import { catalogoRoute } from './catalogo.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(catalogoRoute)],
  declarations: [CatalogoComponent, CatalogoDetailComponent, CatalogoUpdateComponent, CatalogoDeleteDialogComponent],
  entryComponents: [CatalogoDeleteDialogComponent],
})
export class ShoppingonlineCatalogoModule {}
