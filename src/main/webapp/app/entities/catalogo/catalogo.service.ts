import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICatalogo } from 'app/shared/model/catalogo.model';

type EntityResponseType = HttpResponse<ICatalogo>;
type EntityArrayResponseType = HttpResponse<ICatalogo[]>;

@Injectable({ providedIn: 'root' })
export class CatalogoService {
  public resourceUrl = SERVER_API_URL + 'api/catalogos';

  constructor(protected http: HttpClient) {}

  create(catalogo: ICatalogo): Observable<EntityResponseType> {
    return this.http.post<ICatalogo>(this.resourceUrl, catalogo, { observe: 'response' });
  }

  update(catalogo: ICatalogo): Observable<EntityResponseType> {
    return this.http.put<ICatalogo>(this.resourceUrl, catalogo, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICatalogo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICatalogo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
