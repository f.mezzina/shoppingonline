import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICatalogo, Catalogo } from 'app/shared/model/catalogo.model';
import { CatalogoService } from './catalogo.service';
import { CatalogoComponent } from './catalogo.component';
import { CatalogoDetailComponent } from './catalogo-detail.component';
import { CatalogoUpdateComponent } from './catalogo-update.component';

@Injectable({ providedIn: 'root' })
export class CatalogoResolve implements Resolve<ICatalogo> {
  constructor(private service: CatalogoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICatalogo> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((catalogo: HttpResponse<Catalogo>) => {
          if (catalogo.body) {
            return of(catalogo.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Catalogo());
  }
}

export const catalogoRoute: Routes = [
  {
    path: '',
    component: CatalogoComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.catalogo.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CatalogoDetailComponent,
    resolve: {
      catalogo: CatalogoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.catalogo.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CatalogoUpdateComponent,
    resolve: {
      catalogo: CatalogoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.catalogo.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CatalogoUpdateComponent,
    resolve: {
      catalogo: CatalogoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.catalogo.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
