import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICatalogo } from 'app/shared/model/catalogo.model';
import { CatalogoService } from './catalogo.service';

@Component({
  templateUrl: './catalogo-delete-dialog.component.html',
})
export class CatalogoDeleteDialogComponent {
  catalogo?: ICatalogo;

  constructor(protected catalogoService: CatalogoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.catalogoService.delete(id).subscribe(() => {
      this.eventManager.broadcast('catalogoListModification');
      this.activeModal.close();
    });
  }
}
