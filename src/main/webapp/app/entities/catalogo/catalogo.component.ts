import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICatalogo } from 'app/shared/model/catalogo.model';
import { CatalogoService } from './catalogo.service';
import { CatalogoDeleteDialogComponent } from './catalogo-delete-dialog.component';

@Component({
  selector: 'jhi-catalogo',
  templateUrl: './catalogo.component.html',
})
export class CatalogoComponent implements OnInit, OnDestroy {
  catalogos?: ICatalogo[];
  eventSubscriber?: Subscription;

  constructor(protected catalogoService: CatalogoService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.catalogoService.query().subscribe((res: HttpResponse<ICatalogo[]>) => (this.catalogos = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCatalogos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICatalogo): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCatalogos(): void {
    this.eventSubscriber = this.eventManager.subscribe('catalogoListModification', () => this.loadAll());
  }

  delete(catalogo: ICatalogo): void {
    const modalRef = this.modalService.open(CatalogoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.catalogo = catalogo;
  }
}
