import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICatalogo, Catalogo } from 'app/shared/model/catalogo.model';
import { CatalogoService } from './catalogo.service';

@Component({
  selector: 'jhi-catalogo-update',
  templateUrl: './catalogo-update.component.html',
})
export class CatalogoUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected catalogoService: CatalogoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ catalogo }) => {
      this.updateForm(catalogo);
    });
  }

  updateForm(catalogo: ICatalogo): void {
    this.editForm.patchValue({
      id: catalogo.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const catalogo = this.createFromForm();
    if (catalogo.id !== undefined) {
      this.subscribeToSaveResponse(this.catalogoService.update(catalogo));
    } else {
      this.subscribeToSaveResponse(this.catalogoService.create(catalogo));
    }
  }

  private createFromForm(): ICatalogo {
    return {
      ...new Catalogo(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICatalogo>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
