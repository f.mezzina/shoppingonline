import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'azienda',
        loadChildren: () => import('./azienda/azienda.module').then(m => m.ShoppingonlineAziendaModule),
      },
      {
        path: 'utente',
        loadChildren: () => import('./utente/utente.module').then(m => m.ShoppingonlineUtenteModule),
      },
      {
        path: 'operatore-adv',
        loadChildren: () => import('./operatore-adv/operatore-adv.module').then(m => m.ShoppingonlineOperatoreAdvModule),
      },
      {
        path: 'cliente',
        loadChildren: () => import('./cliente/cliente.module').then(m => m.ShoppingonlineClienteModule),
      },
      {
        path: 'profilazione',
        loadChildren: () => import('./profilazione/profilazione.module').then(m => m.ShoppingonlineProfilazioneModule),
      },
      {
        path: 'commessa',
        loadChildren: () => import('./commessa/commessa.module').then(m => m.ShoppingonlineCommessaModule),
      },
      {
        path: 'ordine',
        loadChildren: () => import('./ordine/ordine.module').then(m => m.ShoppingonlineOrdineModule),
      },
      {
        path: 'spedizione',
        loadChildren: () => import('./spedizione/spedizione.module').then(m => m.ShoppingonlineSpedizioneModule),
      },
      {
        path: 'pagamento',
        loadChildren: () => import('./pagamento/pagamento.module').then(m => m.ShoppingonlinePagamentoModule),
      },
      {
        path: 'fattura',
        loadChildren: () => import('./fattura/fattura.module').then(m => m.ShoppingonlineFatturaModule),
      },
      {
        path: 'fatturazione',
        loadChildren: () => import('./fatturazione/fatturazione.module').then(m => m.ShoppingonlineFatturazioneModule),
      },
      {
        path: 'computer',
        loadChildren: () => import('./computer/computer.module').then(m => m.ShoppingonlineComputerModule),
      },
      {
        path: 'configurazione',
        loadChildren: () => import('./configurazione/configurazione.module').then(m => m.ShoppingonlineConfigurazioneModule),
      },
      {
        path: 'elemento',
        loadChildren: () => import('./elemento/elemento.module').then(m => m.ShoppingonlineElementoModule),
      },
      {
        path: 'catalogo',
        loadChildren: () => import('./catalogo/catalogo.module').then(m => m.ShoppingonlineCatalogoModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ShoppingonlineEntityModule {}
