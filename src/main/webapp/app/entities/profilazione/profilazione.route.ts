import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProfilazione, Profilazione } from 'app/shared/model/profilazione.model';
import { ProfilazioneService } from './profilazione.service';
import { ProfilazioneComponent } from './profilazione.component';
import { ProfilazioneDetailComponent } from './profilazione-detail.component';
import { ProfilazioneUpdateComponent } from './profilazione-update.component';

@Injectable({ providedIn: 'root' })
export class ProfilazioneResolve implements Resolve<IProfilazione> {
  constructor(private service: ProfilazioneService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProfilazione> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((profilazione: HttpResponse<Profilazione>) => {
          if (profilazione.body) {
            return of(profilazione.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Profilazione());
  }
}

export const profilazioneRoute: Routes = [
  {
    path: '',
    component: ProfilazioneComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.profilazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProfilazioneDetailComponent,
    resolve: {
      profilazione: ProfilazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.profilazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProfilazioneUpdateComponent,
    resolve: {
      profilazione: ProfilazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.profilazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProfilazioneUpdateComponent,
    resolve: {
      profilazione: ProfilazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.profilazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
