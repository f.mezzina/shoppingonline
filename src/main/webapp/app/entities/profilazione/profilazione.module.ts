import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { ProfilazioneComponent } from './profilazione.component';
import { ProfilazioneDetailComponent } from './profilazione-detail.component';
import { ProfilazioneUpdateComponent } from './profilazione-update.component';
import { ProfilazioneDeleteDialogComponent } from './profilazione-delete-dialog.component';
import { profilazioneRoute } from './profilazione.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(profilazioneRoute)],
  declarations: [ProfilazioneComponent, ProfilazioneDetailComponent, ProfilazioneUpdateComponent, ProfilazioneDeleteDialogComponent],
  entryComponents: [ProfilazioneDeleteDialogComponent],
})
export class ShoppingonlineProfilazioneModule {}
