import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IProfilazione, Profilazione } from 'app/shared/model/profilazione.model';
import { ProfilazioneService } from './profilazione.service';
import { IOperatoreAdv } from 'app/shared/model/operatore-adv.model';
import { OperatoreAdvService } from 'app/entities/operatore-adv/operatore-adv.service';
import { ICliente } from 'app/shared/model/cliente.model';
import { ClienteService } from 'app/entities/cliente/cliente.service';

type SelectableEntity = IOperatoreAdv | ICliente;

@Component({
  selector: 'jhi-profilazione-update',
  templateUrl: './profilazione-update.component.html',
})
export class ProfilazioneUpdateComponent implements OnInit {
  isSaving = false;
  operatoreadvs: IOperatoreAdv[] = [];
  clientes: ICliente[] = [];

  editForm = this.fb.group({
    id: [],
    operatoreAdv: [],
    cliente: [],
  });

  constructor(
    protected profilazioneService: ProfilazioneService,
    protected operatoreAdvService: OperatoreAdvService,
    protected clienteService: ClienteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profilazione }) => {
      this.updateForm(profilazione);

      this.operatoreAdvService
        .query({ filter: 'profilazione-is-null' })
        .pipe(
          map((res: HttpResponse<IOperatoreAdv[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IOperatoreAdv[]) => {
          if (!profilazione.operatoreAdv || !profilazione.operatoreAdv.id) {
            this.operatoreadvs = resBody;
          } else {
            this.operatoreAdvService
              .find(profilazione.operatoreAdv.id)
              .pipe(
                map((subRes: HttpResponse<IOperatoreAdv>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IOperatoreAdv[]) => (this.operatoreadvs = concatRes));
          }
        });

      this.clienteService
        .query({ filter: 'profilazione-is-null' })
        .pipe(
          map((res: HttpResponse<ICliente[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ICliente[]) => {
          if (!profilazione.cliente || !profilazione.cliente.id) {
            this.clientes = resBody;
          } else {
            this.clienteService
              .find(profilazione.cliente.id)
              .pipe(
                map((subRes: HttpResponse<ICliente>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ICliente[]) => (this.clientes = concatRes));
          }
        });
    });
  }

  updateForm(profilazione: IProfilazione): void {
    this.editForm.patchValue({
      id: profilazione.id,
      operatoreAdv: profilazione.operatoreAdv,
      cliente: profilazione.cliente,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const profilazione = this.createFromForm();
    if (profilazione.id !== undefined) {
      this.subscribeToSaveResponse(this.profilazioneService.update(profilazione));
    } else {
      this.subscribeToSaveResponse(this.profilazioneService.create(profilazione));
    }
  }

  private createFromForm(): IProfilazione {
    return {
      ...new Profilazione(),
      id: this.editForm.get(['id'])!.value,
      operatoreAdv: this.editForm.get(['operatoreAdv'])!.value,
      cliente: this.editForm.get(['cliente'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfilazione>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
