import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProfilazione } from 'app/shared/model/profilazione.model';

type EntityResponseType = HttpResponse<IProfilazione>;
type EntityArrayResponseType = HttpResponse<IProfilazione[]>;

@Injectable({ providedIn: 'root' })
export class ProfilazioneService {
  public resourceUrl = SERVER_API_URL + 'api/profilaziones';

  constructor(protected http: HttpClient) {}

  create(profilazione: IProfilazione): Observable<EntityResponseType> {
    return this.http.post<IProfilazione>(this.resourceUrl, profilazione, { observe: 'response' });
  }

  update(profilazione: IProfilazione): Observable<EntityResponseType> {
    return this.http.put<IProfilazione>(this.resourceUrl, profilazione, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProfilazione>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProfilazione[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
