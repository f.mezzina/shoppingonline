import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProfilazione } from 'app/shared/model/profilazione.model';
import { ProfilazioneService } from './profilazione.service';

@Component({
  templateUrl: './profilazione-delete-dialog.component.html',
})
export class ProfilazioneDeleteDialogComponent {
  profilazione?: IProfilazione;

  constructor(
    protected profilazioneService: ProfilazioneService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.profilazioneService.delete(id).subscribe(() => {
      this.eventManager.broadcast('profilazioneListModification');
      this.activeModal.close();
    });
  }
}
