import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProfilazione } from 'app/shared/model/profilazione.model';

@Component({
  selector: 'jhi-profilazione-detail',
  templateUrl: './profilazione-detail.component.html',
})
export class ProfilazioneDetailComponent implements OnInit {
  profilazione: IProfilazione | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profilazione }) => (this.profilazione = profilazione));
  }

  previousState(): void {
    window.history.back();
  }
}
