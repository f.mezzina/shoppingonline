import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProfilazione } from 'app/shared/model/profilazione.model';
import { ProfilazioneService } from './profilazione.service';
import { ProfilazioneDeleteDialogComponent } from './profilazione-delete-dialog.component';

@Component({
  selector: 'jhi-profilazione',
  templateUrl: './profilazione.component.html',
})
export class ProfilazioneComponent implements OnInit, OnDestroy {
  profilaziones?: IProfilazione[];
  eventSubscriber?: Subscription;

  constructor(
    protected profilazioneService: ProfilazioneService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.profilazioneService.query().subscribe((res: HttpResponse<IProfilazione[]>) => (this.profilaziones = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProfilaziones();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProfilazione): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProfilaziones(): void {
    this.eventSubscriber = this.eventManager.subscribe('profilazioneListModification', () => this.loadAll());
  }

  delete(profilazione: IProfilazione): void {
    const modalRef = this.modalService.open(ProfilazioneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.profilazione = profilazione;
  }
}
