import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFattura } from 'app/shared/model/fattura.model';
import { FatturaService } from './fattura.service';

@Component({
  templateUrl: './fattura-delete-dialog.component.html',
})
export class FatturaDeleteDialogComponent {
  fattura?: IFattura;

  constructor(protected fatturaService: FatturaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fatturaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fatturaListModification');
      this.activeModal.close();
    });
  }
}
