import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFattura } from 'app/shared/model/fattura.model';

type EntityResponseType = HttpResponse<IFattura>;
type EntityArrayResponseType = HttpResponse<IFattura[]>;

@Injectable({ providedIn: 'root' })
export class FatturaService {
  public resourceUrl = SERVER_API_URL + 'api/fatturas';

  constructor(protected http: HttpClient) {}

  create(fattura: IFattura): Observable<EntityResponseType> {
    return this.http.post<IFattura>(this.resourceUrl, fattura, { observe: 'response' });
  }

  update(fattura: IFattura): Observable<EntityResponseType> {
    return this.http.put<IFattura>(this.resourceUrl, fattura, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFattura>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFattura[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
