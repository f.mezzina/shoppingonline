import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFattura } from 'app/shared/model/fattura.model';

@Component({
  selector: 'jhi-fattura-detail',
  templateUrl: './fattura-detail.component.html',
})
export class FatturaDetailComponent implements OnInit {
  fattura: IFattura | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fattura }) => (this.fattura = fattura));
  }

  previousState(): void {
    window.history.back();
  }
}
