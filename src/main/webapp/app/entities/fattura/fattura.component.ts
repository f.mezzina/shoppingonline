import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFattura } from 'app/shared/model/fattura.model';
import { FatturaService } from './fattura.service';
import { FatturaDeleteDialogComponent } from './fattura-delete-dialog.component';

@Component({
  selector: 'jhi-fattura',
  templateUrl: './fattura.component.html',
})
export class FatturaComponent implements OnInit, OnDestroy {
  fatturas?: IFattura[];
  eventSubscriber?: Subscription;

  constructor(protected fatturaService: FatturaService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fatturaService.query().subscribe((res: HttpResponse<IFattura[]>) => (this.fatturas = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFatturas();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFattura): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFatturas(): void {
    this.eventSubscriber = this.eventManager.subscribe('fatturaListModification', () => this.loadAll());
  }

  delete(fattura: IFattura): void {
    const modalRef = this.modalService.open(FatturaDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fattura = fattura;
  }
}
