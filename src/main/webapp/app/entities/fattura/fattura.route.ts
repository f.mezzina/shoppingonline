import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFattura, Fattura } from 'app/shared/model/fattura.model';
import { FatturaService } from './fattura.service';
import { FatturaComponent } from './fattura.component';
import { FatturaDetailComponent } from './fattura-detail.component';
import { FatturaUpdateComponent } from './fattura-update.component';

@Injectable({ providedIn: 'root' })
export class FatturaResolve implements Resolve<IFattura> {
  constructor(private service: FatturaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFattura> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fattura: HttpResponse<Fattura>) => {
          if (fattura.body) {
            return of(fattura.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Fattura());
  }
}

export const fatturaRoute: Routes = [
  {
    path: '',
    component: FatturaComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fattura.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FatturaDetailComponent,
    resolve: {
      fattura: FatturaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fattura.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FatturaUpdateComponent,
    resolve: {
      fattura: FatturaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fattura.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FatturaUpdateComponent,
    resolve: {
      fattura: FatturaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fattura.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
