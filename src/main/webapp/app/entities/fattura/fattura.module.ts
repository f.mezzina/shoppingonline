import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { FatturaComponent } from './fattura.component';
import { FatturaDetailComponent } from './fattura-detail.component';
import { FatturaUpdateComponent } from './fattura-update.component';
import { FatturaDeleteDialogComponent } from './fattura-delete-dialog.component';
import { fatturaRoute } from './fattura.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(fatturaRoute)],
  declarations: [FatturaComponent, FatturaDetailComponent, FatturaUpdateComponent, FatturaDeleteDialogComponent],
  entryComponents: [FatturaDeleteDialogComponent],
})
export class ShoppingonlineFatturaModule {}
