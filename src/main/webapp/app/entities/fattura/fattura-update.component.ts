import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFattura, Fattura } from 'app/shared/model/fattura.model';
import { FatturaService } from './fattura.service';

@Component({
  selector: 'jhi-fattura-update',
  templateUrl: './fattura-update.component.html',
})
export class FatturaUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    partitaIva: [],
    ragioneSociale: [],
  });

  constructor(protected fatturaService: FatturaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fattura }) => {
      this.updateForm(fattura);
    });
  }

  updateForm(fattura: IFattura): void {
    this.editForm.patchValue({
      id: fattura.id,
      partitaIva: fattura.partitaIva,
      ragioneSociale: fattura.ragioneSociale,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fattura = this.createFromForm();
    if (fattura.id !== undefined) {
      this.subscribeToSaveResponse(this.fatturaService.update(fattura));
    } else {
      this.subscribeToSaveResponse(this.fatturaService.create(fattura));
    }
  }

  private createFromForm(): IFattura {
    return {
      ...new Fattura(),
      id: this.editForm.get(['id'])!.value,
      partitaIva: this.editForm.get(['partitaIva'])!.value,
      ragioneSociale: this.editForm.get(['ragioneSociale'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFattura>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
