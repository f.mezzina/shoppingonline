import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IElemento, Elemento } from 'app/shared/model/elemento.model';
import { ElementoService } from './elemento.service';
import { IConfigurazione } from 'app/shared/model/configurazione.model';
import { ConfigurazioneService } from 'app/entities/configurazione/configurazione.service';
import { ICatalogo } from 'app/shared/model/catalogo.model';
import { CatalogoService } from 'app/entities/catalogo/catalogo.service';

type SelectableEntity = IConfigurazione | ICatalogo;

@Component({
  selector: 'jhi-elemento-update',
  templateUrl: './elemento-update.component.html',
})
export class ElementoUpdateComponent implements OnInit {
  isSaving = false;
  configuraziones: IConfigurazione[] = [];
  catalogos: ICatalogo[] = [];

  editForm = this.fb.group({
    id: [],
    tipologia: [],
    prezzo: [],
    descrizione: [],
    configurazione: [],
    catalogo: [],
  });

  constructor(
    protected elementoService: ElementoService,
    protected configurazioneService: ConfigurazioneService,
    protected catalogoService: CatalogoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ elemento }) => {
      this.updateForm(elemento);

      this.configurazioneService.query().subscribe((res: HttpResponse<IConfigurazione[]>) => (this.configuraziones = res.body || []));

      this.catalogoService.query().subscribe((res: HttpResponse<ICatalogo[]>) => (this.catalogos = res.body || []));
    });
  }

  updateForm(elemento: IElemento): void {
    this.editForm.patchValue({
      id: elemento.id,
      tipologia: elemento.tipologia,
      prezzo: elemento.prezzo,
      descrizione: elemento.descrizione,
      configurazione: elemento.configurazione,
      catalogo: elemento.catalogo,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const elemento = this.createFromForm();
    if (elemento.id !== undefined) {
      this.subscribeToSaveResponse(this.elementoService.update(elemento));
    } else {
      this.subscribeToSaveResponse(this.elementoService.create(elemento));
    }
  }

  private createFromForm(): IElemento {
    return {
      ...new Elemento(),
      id: this.editForm.get(['id'])!.value,
      tipologia: this.editForm.get(['tipologia'])!.value,
      prezzo: this.editForm.get(['prezzo'])!.value,
      descrizione: this.editForm.get(['descrizione'])!.value,
      configurazione: this.editForm.get(['configurazione'])!.value,
      catalogo: this.editForm.get(['catalogo'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IElemento>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
