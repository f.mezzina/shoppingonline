import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IElemento } from 'app/shared/model/elemento.model';
import { ElementoService } from './elemento.service';

@Component({
  templateUrl: './elemento-delete-dialog.component.html',
})
export class ElementoDeleteDialogComponent {
  elemento?: IElemento;

  constructor(protected elementoService: ElementoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.elementoService.delete(id).subscribe(() => {
      this.eventManager.broadcast('elementoListModification');
      this.activeModal.close();
    });
  }
}
