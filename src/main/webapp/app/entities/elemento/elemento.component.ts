import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IElemento } from 'app/shared/model/elemento.model';
import { ElementoService } from './elemento.service';
import { ElementoDeleteDialogComponent } from './elemento-delete-dialog.component';

@Component({
  selector: 'jhi-elemento',
  templateUrl: './elemento.component.html',
})
export class ElementoComponent implements OnInit, OnDestroy {
  elementos?: IElemento[];
  eventSubscriber?: Subscription;

  constructor(protected elementoService: ElementoService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.elementoService.query().subscribe((res: HttpResponse<IElemento[]>) => (this.elementos = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInElementos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IElemento): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInElementos(): void {
    this.eventSubscriber = this.eventManager.subscribe('elementoListModification', () => this.loadAll());
  }

  delete(elemento: IElemento): void {
    const modalRef = this.modalService.open(ElementoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.elemento = elemento;
  }
}
