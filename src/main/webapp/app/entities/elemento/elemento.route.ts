import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IElemento, Elemento } from 'app/shared/model/elemento.model';
import { ElementoService } from './elemento.service';
import { ElementoComponent } from './elemento.component';
import { ElementoDetailComponent } from './elemento-detail.component';
import { ElementoUpdateComponent } from './elemento-update.component';

@Injectable({ providedIn: 'root' })
export class ElementoResolve implements Resolve<IElemento> {
  constructor(private service: ElementoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IElemento> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((elemento: HttpResponse<Elemento>) => {
          if (elemento.body) {
            return of(elemento.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Elemento());
  }
}

export const elementoRoute: Routes = [
  {
    path: '',
    component: ElementoComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.elemento.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ElementoDetailComponent,
    resolve: {
      elemento: ElementoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.elemento.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ElementoUpdateComponent,
    resolve: {
      elemento: ElementoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.elemento.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ElementoUpdateComponent,
    resolve: {
      elemento: ElementoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.elemento.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
