import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { ElementoComponent } from './elemento.component';
import { ElementoDetailComponent } from './elemento-detail.component';
import { ElementoUpdateComponent } from './elemento-update.component';
import { ElementoDeleteDialogComponent } from './elemento-delete-dialog.component';
import { elementoRoute } from './elemento.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(elementoRoute)],
  declarations: [ElementoComponent, ElementoDetailComponent, ElementoUpdateComponent, ElementoDeleteDialogComponent],
  entryComponents: [ElementoDeleteDialogComponent],
})
export class ShoppingonlineElementoModule {}
