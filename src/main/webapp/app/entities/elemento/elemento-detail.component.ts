import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IElemento } from 'app/shared/model/elemento.model';

@Component({
  selector: 'jhi-elemento-detail',
  templateUrl: './elemento-detail.component.html',
})
export class ElementoDetailComponent implements OnInit {
  elemento: IElemento | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ elemento }) => (this.elemento = elemento));
  }

  previousState(): void {
    window.history.back();
  }
}
