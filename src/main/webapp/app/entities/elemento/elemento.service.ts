import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IElemento } from 'app/shared/model/elemento.model';

type EntityResponseType = HttpResponse<IElemento>;
type EntityArrayResponseType = HttpResponse<IElemento[]>;

@Injectable({ providedIn: 'root' })
export class ElementoService {
  public resourceUrl = SERVER_API_URL + 'api/elementos';

  constructor(protected http: HttpClient) {}

  create(elemento: IElemento): Observable<EntityResponseType> {
    return this.http.post<IElemento>(this.resourceUrl, elemento, { observe: 'response' });
  }

  update(elemento: IElemento): Observable<EntityResponseType> {
    return this.http.put<IElemento>(this.resourceUrl, elemento, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IElemento>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IElemento[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
