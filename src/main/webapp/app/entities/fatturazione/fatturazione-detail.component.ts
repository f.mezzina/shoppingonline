import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFatturazione } from 'app/shared/model/fatturazione.model';

@Component({
  selector: 'jhi-fatturazione-detail',
  templateUrl: './fatturazione-detail.component.html',
})
export class FatturazioneDetailComponent implements OnInit {
  fatturazione: IFatturazione | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fatturazione }) => (this.fatturazione = fatturazione));
  }

  previousState(): void {
    window.history.back();
  }
}
