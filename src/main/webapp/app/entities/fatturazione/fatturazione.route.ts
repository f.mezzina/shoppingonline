import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFatturazione, Fatturazione } from 'app/shared/model/fatturazione.model';
import { FatturazioneService } from './fatturazione.service';
import { FatturazioneComponent } from './fatturazione.component';
import { FatturazioneDetailComponent } from './fatturazione-detail.component';
import { FatturazioneUpdateComponent } from './fatturazione-update.component';

@Injectable({ providedIn: 'root' })
export class FatturazioneResolve implements Resolve<IFatturazione> {
  constructor(private service: FatturazioneService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFatturazione> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fatturazione: HttpResponse<Fatturazione>) => {
          if (fatturazione.body) {
            return of(fatturazione.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Fatturazione());
  }
}

export const fatturazioneRoute: Routes = [
  {
    path: '',
    component: FatturazioneComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fatturazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FatturazioneDetailComponent,
    resolve: {
      fatturazione: FatturazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fatturazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FatturazioneUpdateComponent,
    resolve: {
      fatturazione: FatturazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fatturazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FatturazioneUpdateComponent,
    resolve: {
      fatturazione: FatturazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.fatturazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
