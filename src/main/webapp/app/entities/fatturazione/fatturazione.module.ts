import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { FatturazioneComponent } from './fatturazione.component';
import { FatturazioneDetailComponent } from './fatturazione-detail.component';
import { FatturazioneUpdateComponent } from './fatturazione-update.component';
import { FatturazioneDeleteDialogComponent } from './fatturazione-delete-dialog.component';
import { fatturazioneRoute } from './fatturazione.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(fatturazioneRoute)],
  declarations: [FatturazioneComponent, FatturazioneDetailComponent, FatturazioneUpdateComponent, FatturazioneDeleteDialogComponent],
  entryComponents: [FatturazioneDeleteDialogComponent],
})
export class ShoppingonlineFatturazioneModule {}
