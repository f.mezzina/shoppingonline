import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IFatturazione, Fatturazione } from 'app/shared/model/fatturazione.model';
import { FatturazioneService } from './fatturazione.service';
import { IFattura } from 'app/shared/model/fattura.model';
import { FatturaService } from 'app/entities/fattura/fattura.service';

@Component({
  selector: 'jhi-fatturazione-update',
  templateUrl: './fatturazione-update.component.html',
})
export class FatturazioneUpdateComponent implements OnInit {
  isSaving = false;
  fatturas: IFattura[] = [];

  editForm = this.fb.group({
    id: [],
    fattura: [],
  });

  constructor(
    protected fatturazioneService: FatturazioneService,
    protected fatturaService: FatturaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fatturazione }) => {
      this.updateForm(fatturazione);

      this.fatturaService
        .query({ filter: 'fatturazione-is-null' })
        .pipe(
          map((res: HttpResponse<IFattura[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFattura[]) => {
          if (!fatturazione.fattura || !fatturazione.fattura.id) {
            this.fatturas = resBody;
          } else {
            this.fatturaService
              .find(fatturazione.fattura.id)
              .pipe(
                map((subRes: HttpResponse<IFattura>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFattura[]) => (this.fatturas = concatRes));
          }
        });
    });
  }

  updateForm(fatturazione: IFatturazione): void {
    this.editForm.patchValue({
      id: fatturazione.id,
      fattura: fatturazione.fattura,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fatturazione = this.createFromForm();
    if (fatturazione.id !== undefined) {
      this.subscribeToSaveResponse(this.fatturazioneService.update(fatturazione));
    } else {
      this.subscribeToSaveResponse(this.fatturazioneService.create(fatturazione));
    }
  }

  private createFromForm(): IFatturazione {
    return {
      ...new Fatturazione(),
      id: this.editForm.get(['id'])!.value,
      fattura: this.editForm.get(['fattura'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFatturazione>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IFattura): any {
    return item.id;
  }
}
