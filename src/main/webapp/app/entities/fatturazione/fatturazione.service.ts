import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFatturazione } from 'app/shared/model/fatturazione.model';

type EntityResponseType = HttpResponse<IFatturazione>;
type EntityArrayResponseType = HttpResponse<IFatturazione[]>;

@Injectable({ providedIn: 'root' })
export class FatturazioneService {
  public resourceUrl = SERVER_API_URL + 'api/fatturaziones';

  constructor(protected http: HttpClient) {}

  create(fatturazione: IFatturazione): Observable<EntityResponseType> {
    return this.http.post<IFatturazione>(this.resourceUrl, fatturazione, { observe: 'response' });
  }

  update(fatturazione: IFatturazione): Observable<EntityResponseType> {
    return this.http.put<IFatturazione>(this.resourceUrl, fatturazione, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFatturazione>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFatturazione[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
