import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFatturazione } from 'app/shared/model/fatturazione.model';
import { FatturazioneService } from './fatturazione.service';

@Component({
  templateUrl: './fatturazione-delete-dialog.component.html',
})
export class FatturazioneDeleteDialogComponent {
  fatturazione?: IFatturazione;

  constructor(
    protected fatturazioneService: FatturazioneService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fatturazioneService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fatturazioneListModification');
      this.activeModal.close();
    });
  }
}
