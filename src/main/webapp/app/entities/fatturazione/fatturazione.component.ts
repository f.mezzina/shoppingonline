import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFatturazione } from 'app/shared/model/fatturazione.model';
import { FatturazioneService } from './fatturazione.service';
import { FatturazioneDeleteDialogComponent } from './fatturazione-delete-dialog.component';

@Component({
  selector: 'jhi-fatturazione',
  templateUrl: './fatturazione.component.html',
})
export class FatturazioneComponent implements OnInit, OnDestroy {
  fatturaziones?: IFatturazione[];
  eventSubscriber?: Subscription;

  constructor(
    protected fatturazioneService: FatturazioneService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fatturazioneService.query().subscribe((res: HttpResponse<IFatturazione[]>) => (this.fatturaziones = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFatturaziones();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFatturazione): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFatturaziones(): void {
    this.eventSubscriber = this.eventManager.subscribe('fatturazioneListModification', () => this.loadAll());
  }

  delete(fatturazione: IFatturazione): void {
    const modalRef = this.modalService.open(FatturazioneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fatturazione = fatturazione;
  }
}
