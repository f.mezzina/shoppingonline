import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IConfigurazione } from 'app/shared/model/configurazione.model';

type EntityResponseType = HttpResponse<IConfigurazione>;
type EntityArrayResponseType = HttpResponse<IConfigurazione[]>;

@Injectable({ providedIn: 'root' })
export class ConfigurazioneService {
  public resourceUrl = SERVER_API_URL + 'api/configuraziones';

  constructor(protected http: HttpClient) {}

  create(configurazione: IConfigurazione): Observable<EntityResponseType> {
    return this.http.post<IConfigurazione>(this.resourceUrl, configurazione, { observe: 'response' });
  }

  update(configurazione: IConfigurazione): Observable<EntityResponseType> {
    return this.http.put<IConfigurazione>(this.resourceUrl, configurazione, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IConfigurazione>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IConfigurazione[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
