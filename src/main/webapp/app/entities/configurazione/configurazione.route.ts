import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IConfigurazione, Configurazione } from 'app/shared/model/configurazione.model';
import { ConfigurazioneService } from './configurazione.service';
import { ConfigurazioneComponent } from './configurazione.component';
import { ConfigurazioneDetailComponent } from './configurazione-detail.component';
import { ConfigurazioneUpdateComponent } from './configurazione-update.component';

@Injectable({ providedIn: 'root' })
export class ConfigurazioneResolve implements Resolve<IConfigurazione> {
  constructor(private service: ConfigurazioneService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IConfigurazione> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((configurazione: HttpResponse<Configurazione>) => {
          if (configurazione.body) {
            return of(configurazione.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Configurazione());
  }
}

export const configurazioneRoute: Routes = [
  {
    path: '',
    component: ConfigurazioneComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.configurazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ConfigurazioneDetailComponent,
    resolve: {
      configurazione: ConfigurazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.configurazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ConfigurazioneUpdateComponent,
    resolve: {
      configurazione: ConfigurazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.configurazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ConfigurazioneUpdateComponent,
    resolve: {
      configurazione: ConfigurazioneResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.configurazione.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
