import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConfigurazione } from 'app/shared/model/configurazione.model';

@Component({
  selector: 'jhi-configurazione-detail',
  templateUrl: './configurazione-detail.component.html',
})
export class ConfigurazioneDetailComponent implements OnInit {
  configurazione: IConfigurazione | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ configurazione }) => (this.configurazione = configurazione));
  }

  previousState(): void {
    window.history.back();
  }
}
