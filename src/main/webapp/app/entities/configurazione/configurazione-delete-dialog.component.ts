import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IConfigurazione } from 'app/shared/model/configurazione.model';
import { ConfigurazioneService } from './configurazione.service';

@Component({
  templateUrl: './configurazione-delete-dialog.component.html',
})
export class ConfigurazioneDeleteDialogComponent {
  configurazione?: IConfigurazione;

  constructor(
    protected configurazioneService: ConfigurazioneService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.configurazioneService.delete(id).subscribe(() => {
      this.eventManager.broadcast('configurazioneListModification');
      this.activeModal.close();
    });
  }
}
