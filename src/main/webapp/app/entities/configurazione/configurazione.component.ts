import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IConfigurazione } from 'app/shared/model/configurazione.model';
import { ConfigurazioneService } from './configurazione.service';
import { ConfigurazioneDeleteDialogComponent } from './configurazione-delete-dialog.component';

@Component({
  selector: 'jhi-configurazione',
  templateUrl: './configurazione.component.html',
})
export class ConfigurazioneComponent implements OnInit, OnDestroy {
  configuraziones?: IConfigurazione[];
  eventSubscriber?: Subscription;

  constructor(
    protected configurazioneService: ConfigurazioneService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.configurazioneService.query().subscribe((res: HttpResponse<IConfigurazione[]>) => (this.configuraziones = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInConfiguraziones();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IConfigurazione): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInConfiguraziones(): void {
    this.eventSubscriber = this.eventManager.subscribe('configurazioneListModification', () => this.loadAll());
  }

  delete(configurazione: IConfigurazione): void {
    const modalRef = this.modalService.open(ConfigurazioneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.configurazione = configurazione;
  }
}
