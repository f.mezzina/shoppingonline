import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { ConfigurazioneComponent } from './configurazione.component';
import { ConfigurazioneDetailComponent } from './configurazione-detail.component';
import { ConfigurazioneUpdateComponent } from './configurazione-update.component';
import { ConfigurazioneDeleteDialogComponent } from './configurazione-delete-dialog.component';
import { configurazioneRoute } from './configurazione.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(configurazioneRoute)],
  declarations: [
    ConfigurazioneComponent,
    ConfigurazioneDetailComponent,
    ConfigurazioneUpdateComponent,
    ConfigurazioneDeleteDialogComponent,
  ],
  entryComponents: [ConfigurazioneDeleteDialogComponent],
})
export class ShoppingonlineConfigurazioneModule {}
