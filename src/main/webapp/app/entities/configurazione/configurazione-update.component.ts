import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IConfigurazione, Configurazione } from 'app/shared/model/configurazione.model';
import { ConfigurazioneService } from './configurazione.service';
import { ICatalogo } from 'app/shared/model/catalogo.model';
import { CatalogoService } from 'app/entities/catalogo/catalogo.service';

@Component({
  selector: 'jhi-configurazione-update',
  templateUrl: './configurazione-update.component.html',
})
export class ConfigurazioneUpdateComponent implements OnInit {
  isSaving = false;
  catalogos: ICatalogo[] = [];

  editForm = this.fb.group({
    id: [],
    tipologia: [],
    catalogo: [],
  });

  constructor(
    protected configurazioneService: ConfigurazioneService,
    protected catalogoService: CatalogoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ configurazione }) => {
      this.updateForm(configurazione);

      this.catalogoService.query().subscribe((res: HttpResponse<ICatalogo[]>) => (this.catalogos = res.body || []));
    });
  }

  updateForm(configurazione: IConfigurazione): void {
    this.editForm.patchValue({
      id: configurazione.id,
      tipologia: configurazione.tipologia,
      catalogo: configurazione.catalogo,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const configurazione = this.createFromForm();
    if (configurazione.id !== undefined) {
      this.subscribeToSaveResponse(this.configurazioneService.update(configurazione));
    } else {
      this.subscribeToSaveResponse(this.configurazioneService.create(configurazione));
    }
  }

  private createFromForm(): IConfigurazione {
    return {
      ...new Configurazione(),
      id: this.editForm.get(['id'])!.value,
      tipologia: this.editForm.get(['tipologia'])!.value,
      catalogo: this.editForm.get(['catalogo'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConfigurazione>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICatalogo): any {
    return item.id;
  }
}
