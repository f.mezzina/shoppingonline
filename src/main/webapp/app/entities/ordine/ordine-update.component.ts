import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOrdine, Ordine } from 'app/shared/model/ordine.model';
import { OrdineService } from './ordine.service';
import { IPagamento } from 'app/shared/model/pagamento.model';
import { PagamentoService } from 'app/entities/pagamento/pagamento.service';
import { ISpedizione } from 'app/shared/model/spedizione.model';
import { SpedizioneService } from 'app/entities/spedizione/spedizione.service';
import { IFattura } from 'app/shared/model/fattura.model';
import { FatturaService } from 'app/entities/fattura/fattura.service';
import { IComputer } from 'app/shared/model/computer.model';
import { ComputerService } from 'app/entities/computer/computer.service';
import { ICommessa } from 'app/shared/model/commessa.model';
import { CommessaService } from 'app/entities/commessa/commessa.service';
import { ICliente } from 'app/shared/model/cliente.model';
import { ClienteService } from 'app/entities/cliente/cliente.service';

type SelectableEntity = IPagamento | ISpedizione | IFattura | IComputer | ICommessa | ICliente;

@Component({
  selector: 'jhi-ordine-update',
  templateUrl: './ordine-update.component.html',
})
export class OrdineUpdateComponent implements OnInit {
  isSaving = false;
  pagamentos: IPagamento[] = [];
  spediziones: ISpedizione[] = [];
  fatturas: IFattura[] = [];
  computers: IComputer[] = [];
  commessas: ICommessa[] = [];
  clientes: ICliente[] = [];
  dataDp: any;

  editForm = this.fb.group({
    id: [],
    stato: [],
    numero: [],
    data: [],
    pagamento: [],
    spedizione: [],
    fattura: [],
    computer: [],
    commessa: [],
    cliente: [],
  });

  constructor(
    protected ordineService: OrdineService,
    protected pagamentoService: PagamentoService,
    protected spedizioneService: SpedizioneService,
    protected fatturaService: FatturaService,
    protected computerService: ComputerService,
    protected commessaService: CommessaService,
    protected clienteService: ClienteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ordine }) => {
      this.updateForm(ordine);

      this.pagamentoService
        .query({ filter: 'ordine-is-null' })
        .pipe(
          map((res: HttpResponse<IPagamento[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IPagamento[]) => {
          if (!ordine.pagamento || !ordine.pagamento.id) {
            this.pagamentos = resBody;
          } else {
            this.pagamentoService
              .find(ordine.pagamento.id)
              .pipe(
                map((subRes: HttpResponse<IPagamento>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IPagamento[]) => (this.pagamentos = concatRes));
          }
        });

      this.spedizioneService
        .query({ filter: 'ordine-is-null' })
        .pipe(
          map((res: HttpResponse<ISpedizione[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ISpedizione[]) => {
          if (!ordine.spedizione || !ordine.spedizione.id) {
            this.spediziones = resBody;
          } else {
            this.spedizioneService
              .find(ordine.spedizione.id)
              .pipe(
                map((subRes: HttpResponse<ISpedizione>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ISpedizione[]) => (this.spediziones = concatRes));
          }
        });

      this.fatturaService
        .query({ filter: 'ordine-is-null' })
        .pipe(
          map((res: HttpResponse<IFattura[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFattura[]) => {
          if (!ordine.fattura || !ordine.fattura.id) {
            this.fatturas = resBody;
          } else {
            this.fatturaService
              .find(ordine.fattura.id)
              .pipe(
                map((subRes: HttpResponse<IFattura>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFattura[]) => (this.fatturas = concatRes));
          }
        });

      this.computerService
        .query({ filter: 'ordine-is-null' })
        .pipe(
          map((res: HttpResponse<IComputer[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IComputer[]) => {
          if (!ordine.computer || !ordine.computer.id) {
            this.computers = resBody;
          } else {
            this.computerService
              .find(ordine.computer.id)
              .pipe(
                map((subRes: HttpResponse<IComputer>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IComputer[]) => (this.computers = concatRes));
          }
        });

      this.commessaService.query().subscribe((res: HttpResponse<ICommessa[]>) => (this.commessas = res.body || []));

      this.clienteService.query().subscribe((res: HttpResponse<ICliente[]>) => (this.clientes = res.body || []));
    });
  }

  updateForm(ordine: IOrdine): void {
    this.editForm.patchValue({
      id: ordine.id,
      stato: ordine.stato,
      numero: ordine.numero,
      data: ordine.data,
      pagamento: ordine.pagamento,
      spedizione: ordine.spedizione,
      fattura: ordine.fattura,
      computer: ordine.computer,
      commessa: ordine.commessa,
      cliente: ordine.cliente,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ordine = this.createFromForm();
    if (ordine.id !== undefined) {
      this.subscribeToSaveResponse(this.ordineService.update(ordine));
    } else {
      this.subscribeToSaveResponse(this.ordineService.create(ordine));
    }
  }

  private createFromForm(): IOrdine {
    return {
      ...new Ordine(),
      id: this.editForm.get(['id'])!.value,
      stato: this.editForm.get(['stato'])!.value,
      numero: this.editForm.get(['numero'])!.value,
      data: this.editForm.get(['data'])!.value,
      pagamento: this.editForm.get(['pagamento'])!.value,
      spedizione: this.editForm.get(['spedizione'])!.value,
      fattura: this.editForm.get(['fattura'])!.value,
      computer: this.editForm.get(['computer'])!.value,
      commessa: this.editForm.get(['commessa'])!.value,
      cliente: this.editForm.get(['cliente'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrdine>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
