import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAzienda, Azienda } from 'app/shared/model/azienda.model';
import { AziendaService } from './azienda.service';

@Component({
  selector: 'jhi-azienda-update',
  templateUrl: './azienda-update.component.html',
})
export class AziendaUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nome: [],
    cognome: [],
    agenzia: [],
  });

  constructor(protected aziendaService: AziendaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ azienda }) => {
      this.updateForm(azienda);
    });
  }

  updateForm(azienda: IAzienda): void {
    this.editForm.patchValue({
      id: azienda.id,
      nome: azienda.nome,
      cognome: azienda.cognome,
      agenzia: azienda.agenzia,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const azienda = this.createFromForm();
    if (azienda.id !== undefined) {
      this.subscribeToSaveResponse(this.aziendaService.update(azienda));
    } else {
      this.subscribeToSaveResponse(this.aziendaService.create(azienda));
    }
  }

  private createFromForm(): IAzienda {
    return {
      ...new Azienda(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      cognome: this.editForm.get(['cognome'])!.value,
      agenzia: this.editForm.get(['agenzia'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAzienda>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
