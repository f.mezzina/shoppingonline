import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICommessa, Commessa } from 'app/shared/model/commessa.model';
import { CommessaService } from './commessa.service';
import { CommessaComponent } from './commessa.component';
import { CommessaDetailComponent } from './commessa-detail.component';
import { CommessaUpdateComponent } from './commessa-update.component';

@Injectable({ providedIn: 'root' })
export class CommessaResolve implements Resolve<ICommessa> {
  constructor(private service: CommessaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICommessa> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((commessa: HttpResponse<Commessa>) => {
          if (commessa.body) {
            return of(commessa.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Commessa());
  }
}

export const commessaRoute: Routes = [
  {
    path: '',
    component: CommessaComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.commessa.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CommessaDetailComponent,
    resolve: {
      commessa: CommessaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.commessa.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CommessaUpdateComponent,
    resolve: {
      commessa: CommessaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.commessa.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CommessaUpdateComponent,
    resolve: {
      commessa: CommessaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.commessa.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
