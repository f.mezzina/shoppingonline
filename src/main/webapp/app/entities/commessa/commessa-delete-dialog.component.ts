import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICommessa } from 'app/shared/model/commessa.model';
import { CommessaService } from './commessa.service';

@Component({
  templateUrl: './commessa-delete-dialog.component.html',
})
export class CommessaDeleteDialogComponent {
  commessa?: ICommessa;

  constructor(protected commessaService: CommessaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.commessaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('commessaListModification');
      this.activeModal.close();
    });
  }
}
