import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICommessa } from 'app/shared/model/commessa.model';

type EntityResponseType = HttpResponse<ICommessa>;
type EntityArrayResponseType = HttpResponse<ICommessa[]>;

@Injectable({ providedIn: 'root' })
export class CommessaService {
  public resourceUrl = SERVER_API_URL + 'api/commessas';

  constructor(protected http: HttpClient) {}

  create(commessa: ICommessa): Observable<EntityResponseType> {
    return this.http.post<ICommessa>(this.resourceUrl, commessa, { observe: 'response' });
  }

  update(commessa: ICommessa): Observable<EntityResponseType> {
    return this.http.put<ICommessa>(this.resourceUrl, commessa, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICommessa>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommessa[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
