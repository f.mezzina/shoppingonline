import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICommessa, Commessa } from 'app/shared/model/commessa.model';
import { CommessaService } from './commessa.service';

@Component({
  selector: 'jhi-commessa-update',
  templateUrl: './commessa-update.component.html',
})
export class CommessaUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected commessaService: CommessaService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commessa }) => {
      this.updateForm(commessa);
    });
  }

  updateForm(commessa: ICommessa): void {
    this.editForm.patchValue({
      id: commessa.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commessa = this.createFromForm();
    if (commessa.id !== undefined) {
      this.subscribeToSaveResponse(this.commessaService.update(commessa));
    } else {
      this.subscribeToSaveResponse(this.commessaService.create(commessa));
    }
  }

  private createFromForm(): ICommessa {
    return {
      ...new Commessa(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommessa>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
