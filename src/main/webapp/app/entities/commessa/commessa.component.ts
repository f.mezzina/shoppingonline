import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICommessa } from 'app/shared/model/commessa.model';
import { CommessaService } from './commessa.service';
import { CommessaDeleteDialogComponent } from './commessa-delete-dialog.component';

@Component({
  selector: 'jhi-commessa',
  templateUrl: './commessa.component.html',
})
export class CommessaComponent implements OnInit, OnDestroy {
  commessas?: ICommessa[];
  eventSubscriber?: Subscription;

  constructor(protected commessaService: CommessaService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.commessaService.query().subscribe((res: HttpResponse<ICommessa[]>) => (this.commessas = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCommessas();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICommessa): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCommessas(): void {
    this.eventSubscriber = this.eventManager.subscribe('commessaListModification', () => this.loadAll());
  }

  delete(commessa: ICommessa): void {
    const modalRef = this.modalService.open(CommessaDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.commessa = commessa;
  }
}
