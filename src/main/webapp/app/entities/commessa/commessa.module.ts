import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { CommessaComponent } from './commessa.component';
import { CommessaDetailComponent } from './commessa-detail.component';
import { CommessaUpdateComponent } from './commessa-update.component';
import { CommessaDeleteDialogComponent } from './commessa-delete-dialog.component';
import { commessaRoute } from './commessa.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(commessaRoute)],
  declarations: [CommessaComponent, CommessaDetailComponent, CommessaUpdateComponent, CommessaDeleteDialogComponent],
  entryComponents: [CommessaDeleteDialogComponent],
})
export class ShoppingonlineCommessaModule {}
