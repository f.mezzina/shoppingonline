import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommessa } from 'app/shared/model/commessa.model';

@Component({
  selector: 'jhi-commessa-detail',
  templateUrl: './commessa-detail.component.html',
})
export class CommessaDetailComponent implements OnInit {
  commessa: ICommessa | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commessa }) => (this.commessa = commessa));
  }

  previousState(): void {
    window.history.back();
  }
}
