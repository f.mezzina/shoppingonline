import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingonlineSharedModule } from 'app/shared/shared.module';
import { OperatoreAdvComponent } from './operatore-adv.component';
import { OperatoreAdvDetailComponent } from './operatore-adv-detail.component';
import { OperatoreAdvUpdateComponent } from './operatore-adv-update.component';
import { OperatoreAdvDeleteDialogComponent } from './operatore-adv-delete-dialog.component';
import { operatoreAdvRoute } from './operatore-adv.route';

@NgModule({
  imports: [ShoppingonlineSharedModule, RouterModule.forChild(operatoreAdvRoute)],
  declarations: [OperatoreAdvComponent, OperatoreAdvDetailComponent, OperatoreAdvUpdateComponent, OperatoreAdvDeleteDialogComponent],
  entryComponents: [OperatoreAdvDeleteDialogComponent],
})
export class ShoppingonlineOperatoreAdvModule {}
