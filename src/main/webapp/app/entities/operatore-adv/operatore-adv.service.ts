import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOperatoreAdv } from 'app/shared/model/operatore-adv.model';

type EntityResponseType = HttpResponse<IOperatoreAdv>;
type EntityArrayResponseType = HttpResponse<IOperatoreAdv[]>;

@Injectable({ providedIn: 'root' })
export class OperatoreAdvService {
  public resourceUrl = SERVER_API_URL + 'api/operatore-advs';

  constructor(protected http: HttpClient) {}

  create(operatoreAdv: IOperatoreAdv): Observable<EntityResponseType> {
    return this.http.post<IOperatoreAdv>(this.resourceUrl, operatoreAdv, { observe: 'response' });
  }

  update(operatoreAdv: IOperatoreAdv): Observable<EntityResponseType> {
    return this.http.put<IOperatoreAdv>(this.resourceUrl, operatoreAdv, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOperatoreAdv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOperatoreAdv[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
