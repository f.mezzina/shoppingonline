import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOperatoreAdv, OperatoreAdv } from 'app/shared/model/operatore-adv.model';
import { OperatoreAdvService } from './operatore-adv.service';
import { OperatoreAdvComponent } from './operatore-adv.component';
import { OperatoreAdvDetailComponent } from './operatore-adv-detail.component';
import { OperatoreAdvUpdateComponent } from './operatore-adv-update.component';

@Injectable({ providedIn: 'root' })
export class OperatoreAdvResolve implements Resolve<IOperatoreAdv> {
  constructor(private service: OperatoreAdvService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOperatoreAdv> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((operatoreAdv: HttpResponse<OperatoreAdv>) => {
          if (operatoreAdv.body) {
            return of(operatoreAdv.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OperatoreAdv());
  }
}

export const operatoreAdvRoute: Routes = [
  {
    path: '',
    component: OperatoreAdvComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.operatoreAdv.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OperatoreAdvDetailComponent,
    resolve: {
      operatoreAdv: OperatoreAdvResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.operatoreAdv.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OperatoreAdvUpdateComponent,
    resolve: {
      operatoreAdv: OperatoreAdvResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.operatoreAdv.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OperatoreAdvUpdateComponent,
    resolve: {
      operatoreAdv: OperatoreAdvResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'shoppingonlineApp.operatoreAdv.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
