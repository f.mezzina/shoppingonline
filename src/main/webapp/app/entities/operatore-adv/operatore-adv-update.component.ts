import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOperatoreAdv, OperatoreAdv } from 'app/shared/model/operatore-adv.model';
import { OperatoreAdvService } from './operatore-adv.service';
import { IUtente } from 'app/shared/model/utente.model';
import { UtenteService } from 'app/entities/utente/utente.service';
import { IAzienda } from 'app/shared/model/azienda.model';
import { AziendaService } from 'app/entities/azienda/azienda.service';

type SelectableEntity = IUtente | IAzienda;

@Component({
  selector: 'jhi-operatore-adv-update',
  templateUrl: './operatore-adv-update.component.html',
})
export class OperatoreAdvUpdateComponent implements OnInit {
  isSaving = false;
  utentes: IUtente[] = [];
  aziendas: IAzienda[] = [];

  editForm = this.fb.group({
    id: [],
    nome: [],
    cognome: [],
    agenzia: [],
    username: [],
    password: [],
    autenticato: [],
    utente: [],
    azienda: [],
  });

  constructor(
    protected operatoreAdvService: OperatoreAdvService,
    protected utenteService: UtenteService,
    protected aziendaService: AziendaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ operatoreAdv }) => {
      this.updateForm(operatoreAdv);

      this.utenteService
        .query({ filter: 'operatoreadv-is-null' })
        .pipe(
          map((res: HttpResponse<IUtente[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IUtente[]) => {
          if (!operatoreAdv.utente || !operatoreAdv.utente.id) {
            this.utentes = resBody;
          } else {
            this.utenteService
              .find(operatoreAdv.utente.id)
              .pipe(
                map((subRes: HttpResponse<IUtente>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IUtente[]) => (this.utentes = concatRes));
          }
        });

      this.aziendaService.query().subscribe((res: HttpResponse<IAzienda[]>) => (this.aziendas = res.body || []));
    });
  }

  updateForm(operatoreAdv: IOperatoreAdv): void {
    this.editForm.patchValue({
      id: operatoreAdv.id,
      nome: operatoreAdv.nome,
      cognome: operatoreAdv.cognome,
      agenzia: operatoreAdv.agenzia,
      username: operatoreAdv.username,
      password: operatoreAdv.password,
      autenticato: operatoreAdv.autenticato,
      utente: operatoreAdv.utente,
      azienda: operatoreAdv.azienda,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const operatoreAdv = this.createFromForm();
    if (operatoreAdv.id !== undefined) {
      this.subscribeToSaveResponse(this.operatoreAdvService.update(operatoreAdv));
    } else {
      this.subscribeToSaveResponse(this.operatoreAdvService.create(operatoreAdv));
    }
  }

  private createFromForm(): IOperatoreAdv {
    return {
      ...new OperatoreAdv(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      cognome: this.editForm.get(['cognome'])!.value,
      agenzia: this.editForm.get(['agenzia'])!.value,
      username: this.editForm.get(['username'])!.value,
      password: this.editForm.get(['password'])!.value,
      autenticato: this.editForm.get(['autenticato'])!.value,
      utente: this.editForm.get(['utente'])!.value,
      azienda: this.editForm.get(['azienda'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOperatoreAdv>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
