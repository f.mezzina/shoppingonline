import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOperatoreAdv } from 'app/shared/model/operatore-adv.model';
import { OperatoreAdvService } from './operatore-adv.service';
import { OperatoreAdvDeleteDialogComponent } from './operatore-adv-delete-dialog.component';

@Component({
  selector: 'jhi-operatore-adv',
  templateUrl: './operatore-adv.component.html',
})
export class OperatoreAdvComponent implements OnInit, OnDestroy {
  operatoreAdvs?: IOperatoreAdv[];
  eventSubscriber?: Subscription;

  constructor(
    protected operatoreAdvService: OperatoreAdvService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.operatoreAdvService.query().subscribe((res: HttpResponse<IOperatoreAdv[]>) => (this.operatoreAdvs = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOperatoreAdvs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOperatoreAdv): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOperatoreAdvs(): void {
    this.eventSubscriber = this.eventManager.subscribe('operatoreAdvListModification', () => this.loadAll());
  }

  delete(operatoreAdv: IOperatoreAdv): void {
    const modalRef = this.modalService.open(OperatoreAdvDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.operatoreAdv = operatoreAdv;
  }
}
