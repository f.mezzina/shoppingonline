import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOperatoreAdv } from 'app/shared/model/operatore-adv.model';

@Component({
  selector: 'jhi-operatore-adv-detail',
  templateUrl: './operatore-adv-detail.component.html',
})
export class OperatoreAdvDetailComponent implements OnInit {
  operatoreAdv: IOperatoreAdv | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ operatoreAdv }) => (this.operatoreAdv = operatoreAdv));
  }

  previousState(): void {
    window.history.back();
  }
}
