import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOperatoreAdv } from 'app/shared/model/operatore-adv.model';
import { OperatoreAdvService } from './operatore-adv.service';

@Component({
  templateUrl: './operatore-adv-delete-dialog.component.html',
})
export class OperatoreAdvDeleteDialogComponent {
  operatoreAdv?: IOperatoreAdv;

  constructor(
    protected operatoreAdvService: OperatoreAdvService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.operatoreAdvService.delete(id).subscribe(() => {
      this.eventManager.broadcast('operatoreAdvListModification');
      this.activeModal.close();
    });
  }
}
